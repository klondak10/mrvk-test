package pro.mrvk.hiver.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.google.gson.JsonObject;

import butterknife.BindView;
import butterknife.OnClick;
import io.fabric.sdk.android.Fabric;
import pro.mrvk.hiver.BuildConfig;
import pro.mrvk.hiver.R;

import java.net.UnknownHostException;

import br.com.sapereaude.maskedEditText.MaskedEditText;
import butterknife.ButterKnife;
import pro.mrvk.hiver.Configuration;
import pro.mrvk.hiver.core.API;
import pro.mrvk.hiver.core.ICallback;
import pro.mrvk.hiver.network.dto.LoginReqDto;
import pro.mrvk.hiver.network.dto.LoginResDto;
import pro.mrvk.hiver.services.LocationService;
import pro.mrvk.hiver.services.NetworkService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static pro.mrvk.hiver.core.PrefencesApi.KEY_AUTH_TOKEN;
import static pro.mrvk.hiver.core.PrefencesApi.getPrefString;
import static pro.mrvk.hiver.core.PrefencesApi.setPrefValue;

public class LoginActivity extends AppCompatActivity {

    static private final String TAG = "MRVK_LoginActv";

    @BindView(R.id.editPhone)
    MaskedEditText editPhone;
    @BindView(R.id.editLogin)
    EditText editLogin;
    @BindView(R.id.editPass)
    EditText editPass;
    @BindView(R.id.versionLabel)
    TextView versionLabel;

    ProgressDialog processDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate");
        Fabric.with(this, new Crashlytics());

        try {
            setContentView(R.layout.activity_login);
            ButterKnife.bind(this);
            processDialog = new ProgressDialog(this);
            processDialog.setMessage(getResources().getString(R.string.authorization));

            /*
             * Debug Solution
             * Настройте следующие реквизиты, чтобы выполнить авторизацию
             */
            if (BuildConfig.DEBUG) {
//                editPhone.setText("2342342344");
//                editLogin.setText("mobile_test");
//                editPass.setText("mobile_1234");
                editPhone.setText("0506840999");
                editLogin.setText("klondaik");
                editPass.setText("qwerty123");
//                onLoginClick();
            }
            versionLabel.setText("v" + BuildConfig.VERSION_NAME);
        } catch (Exception e) {
            e.printStackTrace();
            Crashlytics.logException(e);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        LocationService.start(this);
    }

    /**
     * Click on Login button
     */
    @OnClick(R.id.btnLogin)
    public void onLoginClick() {
        // информация об устройстве
        JsonObject deviceInfo = new JsonObject();
        deviceInfo.addProperty("mrvkVerCode", BuildConfig.VERSION_CODE);
        deviceInfo.addProperty("mrvkVerName", BuildConfig.VERSION_NAME);
        deviceInfo.addProperty("manufacturer", Build.MANUFACTURER);
        deviceInfo.addProperty("model", Build.MODEL);
        deviceInfo.addProperty("brand", Build.BRAND);
        deviceInfo.addProperty("device", Build.DEVICE);
        deviceInfo.addProperty("board", Build.BOARD);
        deviceInfo.addProperty("hardware", Build.HARDWARE);
        deviceInfo.addProperty("host", Build.HOST);
        deviceInfo.addProperty("id", Build.ID);
        deviceInfo.addProperty("product", Build.PRODUCT);
        deviceInfo.addProperty("user", Build.USER);
        deviceInfo.addProperty("verSdk", Build.VERSION.SDK_INT);
        deviceInfo.addProperty("verCodeName", Build.VERSION.CODENAME);
        deviceInfo.addProperty("verRelease", Build.VERSION.RELEASE);
        // реквизиты
        String phone = "+7" + editPhone.getText();
        final String login = editLogin.getText().toString();
        String password = editPass.getText().toString();
        LoginReqDto loginReqDto = new LoginReqDto(phone, login, password, deviceInfo.toString());
        Crashlytics.setString("Auth Phone", phone);
        Crashlytics.setString("Auth Login", login);
        // send request
        processDialog.show();
        NetworkService.getInstance().getPublicApi()
                .postLogin(loginReqDto)
                .enqueue(new Callback<LoginResDto>() {
                    @Override
                    public void onResponse(Call<LoginResDto> call, Response<LoginResDto> response) {
                        processDialog.hide();
                        LoginResDto resDto = response.body();
                        if (resDto == null) {
                            API.showToast("Ошибка авторизации: Ошибка на сервере", 5);
                            return;
                        }
                        if (resDto.getError() != null) {
                            API.showToast("Ошибка авторизации: " + resDto.getError(), 5);
                            return;
                        }
                        String authToken = resDto.getToken();
                        if (authToken == null || authToken.isEmpty()) {
                            API.showToast("Ошибка авторизации: Сервер вернул пустой токен", 5);
                            return;
                        }
                        Crashlytics.setString("Auth Token", authToken);
                        Crashlytics.log("User was authorize with token :: " + authToken);
                        setPrefValue(KEY_AUTH_TOKEN, authToken);

                        // после авторизации снова обновляем конфиг для пользователя
                        processDialog.show();
                        Configuration.updateConfiguration(new ICallback() {
                            @Override
                            public void callback() {
                                processDialog.hide();
                                startActivity(new Intent(getApplicationContext(), PlacesActivity.class)
                                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                                finish();
                            }
                        });
                    }

                    @Override
                    public void onFailure(Call<LoginResDto> call, Throwable t) {
                        processDialog.hide();
                        t.printStackTrace();
                        if (t instanceof UnknownHostException) {
                            // все нормально, нет сети
                            String token = getPrefString(KEY_AUTH_TOKEN);
                            if (token != null && !token.isEmpty()) {
                                startActivity(new Intent(getApplicationContext(), PlacesActivity.class)
                                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                                finish();
                            } else {
                                API.showToast("Ошибка авторизации: Нет сети", 3);
                            }
                            return;
                        }
                        Crashlytics.logException(t);
                        API.showToast("CRASH: Ошибка авторизации", 5);
                    }
                });
    }

    /**
     * Goto Registration Web page
     */
    @OnClick(R.id.btnReg)
    public void onRegClick() {
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://mrvk.pro")));
        } catch (Exception e) {
            e.printStackTrace();
            Crashlytics.logException(e);
        }
    }

    /*
    void parseAuth(JsonObject jsonObject) {
        try {
            processDialog.hide();
            JsonElement error = jsonObject.get("error");
            if (error == null) {
                String authToken = jsonObject.get("token").getAsString();
                Crashlytics.setString("Auth Token", authToken);
                Crashlytics.log("User was authorize with token :: " + authToken);
                PrefencesApi.setPrefValue(KEY_AUTH_TOKEN, authToken);
                if (DatabaseService.countVisitsRows() > 0) {
                    DatabaseService.logout();
                }
                startActivity(new Intent(getApplicationContext(), PlacesActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                finish();

                // после авторизации снова получаем конфиг для пользователя
//                ConfigurationApi.updateConfiguration(this, new ICallback() {
//                    @Override
//                    public void callback() {
//                        startActivity(new Intent(getApplicationContext(), PlacesActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
//                        finish();
//                    }
//                });
            } else {
                API.showToast("Ошибка авторизации. Проверьте правильность введенных данных.", 1);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Crashlytics.logException(e);
            API.showToast("Ошибка при отправке данных. Отсутствует подключние к серверу.", 1);
        }
    }
    */
}
