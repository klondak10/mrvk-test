package pro.mrvk.hiver.models;

import android.content.Intent;
import android.graphics.drawable.Drawable;

public class ApplicationMapInfo {

    private Drawable iconDrawable;
    private String label;
    private Intent intent;

    public Drawable getIconDrawable() {
        return iconDrawable;
    }

    public void setIconDrawable(Drawable iconDrawable) {
        this.iconDrawable = iconDrawable;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Intent getIntent() {
        return intent;
    }

    public void setIntent(Intent intent) {
        this.intent = intent;
    }
}
