package pro.mrvk.hiver.models;

public class VisitPhotoValue extends VisitValue {

    public VisitPhotoValue(String value) {
        type = PHOTO_TYPE;
        stringValue = value;
        booleanValue = false;
    }

    public VisitPhotoValue(String value, boolean isSended) {
        type = PHOTO_TYPE;
        stringValue = value;
        booleanValue = isSended;
    }

    public String getPathName() {
        return stringValue;
    }

    public boolean isSending() {
        return booleanValue;
    }

    public void setSending(boolean isSended) {
        booleanValue = isSended;
    }
}
