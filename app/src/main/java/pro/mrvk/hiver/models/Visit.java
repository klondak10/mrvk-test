package pro.mrvk.hiver.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.HashMap;
import java.util.Set;

public class Visit {

    @Expose
    @SerializedName("id")
    private String id;

    @Expose
    @SerializedName("place")
    private VisitPlace place;

    @Expose
    @SerializedName("template")
    private VisitTemplate template;

    @Expose
    @SerializedName("reportDate")
    private String reportDate;

    @Expose
    @SerializedName("status")
    private VisitStatus status;

    @Expose
    @SerializedName("availableToReport")
    private boolean availableToReport;

    @Expose
    @SerializedName("isCheckGeo")
    private boolean isCheckGeo;

    @Expose
    @SerializedName("startTime")
    private String startTime;

    @Expose
    @SerializedName("finishTime")
    private String finishTime;

    @Expose
    @SerializedName("startPosition")
    private String startPosition;

    @Expose
    @SerializedName("finishPosition")
    private String finishPosition;

    @Expose
    @SerializedName("values")
    private HashMap<String, VisitValue> values;

    @Expose
    @SerializedName("is_quering")
    private boolean isQuering;

    @Expose
    @SerializedName("updatedAt")
    private String updatedAt;

    public Visit(String id, VisitPlace place, VisitTemplate template, String reportDate, VisitStatus status, boolean availableToReport, boolean isCheckGeo, String startTime, String finishTime, String startPosition, String finishPosition, HashMap<String, VisitValue> values, boolean isQuering, String updatedAt) {
        this.id = id;
        this.place = place;
        this.template = template;
        this.reportDate = reportDate;
        this.status = status;
        this.availableToReport = availableToReport;
        this.isCheckGeo = isCheckGeo;
        this.startTime = startTime;
        this.finishTime = finishTime;
        this.startPosition = startPosition;
        this.finishPosition = finishPosition;
        this.values = values;
        this.isQuering = isQuering;
        this.updatedAt = updatedAt;
    }

    public void initValues() {
        HashMap<String, VisitValue> newValues = new HashMap<>();
        if (values != null) {
            Set<String> keys = values.keySet();
            for (String key : keys) {
                VisitValue value = values.get(key).get();
                newValues.put(key, value);
            }
            this.values.clear();
        }
        this.values = newValues;
    }

    public String getId() {
        return id;
    }

    public VisitPlace getPlace() {
        return place;
    }

    public VisitTemplate getTemplate() {
        return template;
    }

    public String getReportDate() {
        return reportDate;
    }

    public void setReportDate(String reportDate) {
        this.reportDate = reportDate;
    }

    public VisitStatus getStatus() {
        return status;
    }

    public void setStatus(VisitStatus status) {
        this.status = status;
    }

    public boolean isAvailableToReport() {
        return availableToReport;
    }

    public boolean isCheckGeo() {
        return isCheckGeo;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getFinishTime() {
        return finishTime;
    }

    public void setFinishTime(String finishTime) {
        this.finishTime = finishTime;
    }

    public String getStartPosition() {
        return startPosition;
    }

    public void setStartPosition(String startPosition) {
        this.startPosition = startPosition;
    }

    public String getFinishPosition() {
        return finishPosition;
    }

    public void setFinishPosition(String finishPosition) {
        this.finishPosition = finishPosition;
    }

    public HashMap<String, VisitValue> getValues() {
        if (values == null) {
            values = new HashMap<>();
        }
        return values;
    }

    public String getStringValue(String fieldId) {
        if (!getValues().containsKey(fieldId))
            return null;
        VisitValue value = getValues().get(fieldId);
        if (value instanceof VisitStringValue)
            return ((VisitStringValue) value).getValue();
        return null;
    }

    public Double getDoubleValue(String fieldId) {
        if (!getValues().containsKey(fieldId))
            return null;
        VisitValue value = getValues().get(fieldId);
        if (value instanceof VisitDoubleValue)
            return ((VisitDoubleValue) value).getValue();
        return null;
    }

    public Boolean getBooleanValue(String fieldId) {
        if (!getValues().containsKey(fieldId))
            return null;
        VisitValue value = getValues().get(fieldId);
        if (value instanceof VisitBooleanValue)
            return ((VisitBooleanValue) value).getValue();
        return null;
    }

    public VisitPhotoValue getPhotoValue(String fieldId) {
        if (!getValues().containsKey(fieldId))
            return null;
        VisitValue value = getValues().get(fieldId);
        if (value instanceof VisitPhotoValue)
            return (VisitPhotoValue) value;
        return null;
    }

    public boolean isQuering() {
        return isQuering;
    }

    public void setQuering(boolean quering) {
        isQuering = quering;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }
}
