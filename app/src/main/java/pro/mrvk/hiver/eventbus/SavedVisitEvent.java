package pro.mrvk.hiver.eventbus;

import pro.mrvk.hiver.models.Visit;

public class SavedVisitEvent {

    private Visit visit;

    public SavedVisitEvent(Visit visit) {
        this.visit = visit;
    }

    public Visit getVisit() {
        return visit;
    }
}
