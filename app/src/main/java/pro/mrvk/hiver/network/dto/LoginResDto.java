package pro.mrvk.hiver.network.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginResDto extends BaseResDto {
    @Expose
    @SerializedName("token")
    private String token;

    public String getToken() {
        return token;
    }
}
