package pro.mrvk.hiver.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.ogaclejapan.smarttablayout.SmartTabLayout;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItemAdapter;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems;

import io.fabric.sdk.android.Fabric;
import pro.mrvk.hiver.R;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import pro.mrvk.hiver.helpers.PhotoManager;
import pro.mrvk.hiver.models.Visit;
import pro.mrvk.hiver.services.DatabaseService;
import pro.mrvk.hiver.services.LocationService;
import pro.mrvk.hiver.ui.fragments.PlacesFragment;

public class PlacesActivity extends AppCompatActivity {

    static private final String TAG = "MRVK_PlacesActv";

    @BindView(R.id.viewPagerTab)
    SmartTabLayout viewPagerTab;
    @BindView(R.id.viewpager)
    ViewPager viewpager;

    private PhotoManager photoManager = PhotoManager.get();
    private FragmentPagerItemAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate");
        Fabric.with(this, new Crashlytics());

        try {
            SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());

            Visit visit = DatabaseService.getInProgressVisit(format1.format(new Date()));
            if (visit != null) {
                VisitActivity.open(visit, this);
            }
            setContentView(R.layout.activity_places);
            ButterKnife.bind(this);
            getSupportActionBar().setElevation(0);
            FragmentPagerItems.Creator creator = FragmentPagerItems.with(getApplicationContext());
            Date now = new Date();
            int oneDay = 24 * 3600 * 1000;
            now.setTime(now.getTime() - 8 * oneDay);
            SimpleDateFormat format = new SimpleDateFormat("d MMM", Locale.getDefault());
            for (int i = 0; i < 15; i++) {
                now.setTime(now.getTime() + oneDay);
                String name = format.format(now);
                Bundle bundle = new Bundle();
                bundle.putString("date", format1.format(now));
                creator.add(name, PlacesFragment.class, bundle);
            }
            FragmentPagerItems items = creator.create();
            adapter = new FragmentPagerItemAdapter(
                    getSupportFragmentManager(), items);
            viewpager.setAdapter(adapter);
            viewPagerTab.setViewPager(viewpager);
            for (int i = 0; i < items.size(); i++) {
                setFont(viewPagerTab.getTabAt(i));
            }
            ((TextView) viewPagerTab.getTabAt(7)).setTextColor(0xffffffff);
            viewpager.setCurrentItem(7);
            viewPagerTab.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                }

                @Override
                public void onPageSelected(int position) {
                    for (int i = 0; i < 15; i++)
                        ((TextView) viewPagerTab.getTabAt(i)).setTextColor((position == i ? 0xffffffff : 0xffe1e1e1));
                    PlacesFragment frag = (PlacesFragment) adapter.getPage(viewpager.getCurrentItem());
                    try {
                        if (photoManager.isSending()) {
                            frag.orangeSyncButton.setVisibility(View.GONE);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onPageScrollStateChanged(int state) {
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            Crashlytics.logException(e);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        LocationService.start(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            PlacesFragment frag = (PlacesFragment) adapter.getPage(viewpager.getCurrentItem());
            frag.loadVisitRoute();
            if (requestCode == 1 && resultCode == RESULT_OK) {
                int cur = viewpager.getCurrentItem();
                FragmentPagerItems.Creator creator = FragmentPagerItems.with(getApplicationContext());
                Date now = new Date();
                now.setTime(now.getTime() - 1000 * 60 * 60 * 24 * 8);
                SimpleDateFormat format = new SimpleDateFormat("d MMM", Locale.getDefault());
                SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
                for (int i = 0; i < 15; i++) {
                    now.setTime(now.getTime() + 1000 * 60 * 60 * 24);
                    String name = format.format(now);
                    Bundle bundle = new Bundle();
                    bundle.putString("date", format1.format(now));
                    creator.add(name, PlacesFragment.class, bundle);
                }
                FragmentPagerItems items = creator.create();
                adapter = new FragmentPagerItemAdapter(
                        getSupportFragmentManager(), items);
                viewpager.setAdapter(adapter);
                viewPagerTab.setViewPager(viewpager);
                for (int i = 0; i < items.size(); i++) {
                    setFont(viewPagerTab.getTabAt(i));
                }
                ((TextView) viewPagerTab.getTabAt(cur)).setTextColor(0xffffffff);
                viewpager.setCurrentItem(cur);
                viewPagerTab.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                    @Override
                    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                    }

                    @Override
                    public void onPageSelected(int position) {
                        for (int i = 0; i < 15; i++)
                            ((TextView) viewPagerTab.getTabAt(i)).setTextColor((position == i ? 0xffffffff : 0xffe1e1e1));
                        PlacesFragment frag = (PlacesFragment) adapter.getPage(viewpager.getCurrentItem());
                        try {
                            if (photoManager.isSending()) {
                                frag.orangeSyncButton.setVisibility(View.GONE);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onPageScrollStateChanged(int state) {
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
            Crashlytics.logException(e);
        }
    }

    void setFont(View view) {
        Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/HelveticaRegular.ttf");
        ((TextView) view).setTypeface(tf);
    }
}
