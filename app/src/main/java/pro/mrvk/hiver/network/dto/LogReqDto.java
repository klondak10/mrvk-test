package pro.mrvk.hiver.network.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LogReqDto {
    @Expose
    @SerializedName("type")
    private String type;

    @Expose
    @SerializedName("message")
    private String message;

    @Expose
    @SerializedName("reason")
    private String reason;

    public LogReqDto(String type, String message, String reason) {
        this.type = type;
        this.message = message;
        this.reason = reason;
    }

    public LogReqDto(String type, String message) {
        this.type = type;
        this.message = message;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
