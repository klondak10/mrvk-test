package pro.mrvk.hiver.ui.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import pro.mrvk.hiver.R;
import pro.mrvk.hiver.models.ApplicationMapInfo;

public class AppInfoAdapter extends RecyclerView.Adapter<AppInfoAdapter.ViewHolder> {

    private List<ApplicationMapInfo> mListAppInfo;
    private OnClickListener onClickListener = null;

    public AppInfoAdapter(List<ApplicationMapInfo> mListAppInfo) {
        this.mListAppInfo = mListAppInfo;
    }

    public void setOnClickListener(OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    public interface OnClickListener {
        void onItemClick(View view, int pos);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.bottom_sheet_item, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {
        ApplicationMapInfo entry = mListAppInfo.get(i);
        viewHolder.iconImageView.setImageDrawable(entry.getIconDrawable());
        viewHolder.nameTextView.setText(entry.getLabel());
        viewHolder.lyt_parent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickListener.onItemClick(v, i);
            }
        });
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return mListAppInfo.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView iconImageView;
        public TextView nameTextView;
        public View lyt_parent;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            iconImageView = itemView.findViewById(R.id.imageViewIcon);
            nameTextView = itemView.findViewById(R.id.textViewName);
            lyt_parent = itemView.findViewById(R.id.lyt_parent);
        }
    }
}
