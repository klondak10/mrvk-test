package pro.mrvk.hiver.network.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import pro.mrvk.hiver.models.LocationPoint;
import pro.mrvk.hiver.models.LocationPointList;

public class LocationPointListReqDto {
    @Expose
    @SerializedName("points")
    private List<LocationPointReqDto> locationPoints;

    private LocationPointListReqDto() {
    }

    static public LocationPointListReqDto of(LocationPointList locationPoints) {
        LocationPointListReqDto dto = new LocationPointListReqDto();
        dto.locationPoints = new ArrayList<>();
        for (LocationPoint locationPoint: locationPoints.getLocationPoints()){
            dto.locationPoints.add(LocationPointReqDto.of(locationPoint));
        }
        return dto;
    }
}
