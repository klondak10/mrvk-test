package pro.mrvk.hiver.ui.adapters;

import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindDrawable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pro.mrvk.hiver.BuildConfig;
import pro.mrvk.hiver.R;
import pro.mrvk.hiver.core.API;
import pro.mrvk.hiver.models.LocationPoint;
import pro.mrvk.hiver.models.UploadVisitsProgress;
import pro.mrvk.hiver.models.Visit;
import pro.mrvk.hiver.models.VisitPlace;
import pro.mrvk.hiver.services.LocationService;

public class VisitAdapter extends RecyclerView.Adapter<VisitAdapter.ViewHolder> {

    private RecyclerView mRecyclerView;

    private List<Visit> mList = new ArrayList<>();
    private Callback mCallback;

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindDrawable(R.drawable.circle_back_red_left)
        Drawable colorRed;
        @BindDrawable(R.drawable.circle_back_org_left)
        Drawable colorOrange;
        @BindDrawable(R.drawable.circle_back_grn_left)
        Drawable colorGreen;
        @BindDrawable(R.drawable.circle_back_black_left)
        Drawable colorBlack;

        @BindView(R.id.labelAddress)
        TextView labelAddress;
        @BindView(R.id.labelName)
        TextView labelName;
        @BindView(R.id.labelDistance)
        TextView labelDistance;
        @BindView(R.id.colored)
        View colored;
        @BindView(R.id.imgGeoPlace)
        ImageView imgGeo;
        @BindView(R.id.progressBar)
        ProgressBar progressBar;

        private int mProgress;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.visitListItem)
        public void onClickItem() {
            Visit visit = mList.get(getAdapterPosition());
            mCallback.onClickItem(visit);
        }

        public void updateProgress(int progress) {
            mProgress = progress;
            progressBar.setProgress(progress);
            checkShowingProgressBar();
        }

        public void resetProgress() {
            mProgress = 0;
            progressBar.setVisibility(View.GONE);
        }

        public void checkShowingProgressBar() {
            progressBar.setVisibility(needHideProgressBar() ? View.GONE : View.VISIBLE);
        }

        private boolean needHideProgressBar() {
            return mProgress == 0 || mProgress >= 100;
        }
    }

    public VisitAdapter(Callback callback) {
        mCallback = callback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_visit, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final Visit visit = mList.get(position);
        VisitPlace place = visit.getPlace();

        holder.imgGeo.setVisibility(visit.isCheckGeo() ? View.VISIBLE : View.GONE);
        holder.labelAddress.setText(place.getAddress());

        String name = place.getName() + ", " + place.getOrgName();
        holder.labelName.setText(name);

        switch (visit.getStatus().getCode()) {
            case 0:
                holder.colored.setBackground(holder.colorRed);
                break;
            case 2:
                holder.colored.setBackground(holder.colorOrange);
                break;
            case 3:
                holder.colored.setBackground(holder.colorGreen);
                break;
            default:
                holder.colored.setBackground(holder.colorBlack);
        }

        holder.resetProgress();

        if (BuildConfig.DEBUG) {
            Double distance = null;
            LocationPoint lastLocationPoint = LocationService.getLastLocationPoint();
            if (lastLocationPoint != null && place.getLocation() != null) {
                String[] location = place.getLocation().split(",");
                if (location.length == 2) {
                    double lat1 = Double.parseDouble(location[0]);
                    double lon1 = Double.parseDouble(location[1]);
                    double lat2 = lastLocationPoint.getLatitude();
                    double lon2 = lastLocationPoint.getLongitude();
                    distance = API.calculateDistance(lat1, lon1, lat2, lon2);
                }
            }

            String distanceText;

            if (distance == null) {
                distanceText = "?";
            } else if (distance < 10_000) {
                distanceText = "" + Math.round(distance) + " м";
            } else if (distance < 10_000_000) {
                distanceText = String.format(Locale.getDefault(), "%.2f км", distance / 1_000);
            } else {
                distanceText = String.format(Locale.getDefault(), "%.2f тыс. км", distance / 1_000_000);
            }

            holder.labelDistance.setVisibility(View.VISIBLE);
            holder.labelDistance.setText(distanceText);
        } else {
            holder.labelDistance.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return mList == null ? 0 : mList.size();
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        mRecyclerView = recyclerView;
    }

    public void setList(List<Visit> list) {
        mList = list;
        sortData();
    }

    private void sortData() {
        Collections.sort(mList, new Comparator<Visit>() {
            @Override
            public int compare(Visit v1, Visit v2) {
                Integer s1 = v1.getStatus().getCode();
                Integer s2 = v2.getStatus().getCode();
                int sComp = s1.compareTo(s2);
                if (sComp != 0) {
                    return sComp;
                }

                String d1 = v1.getUpdatedAt();
                String d2 = v2.getUpdatedAt();
                int result = 0;

                if (d1 == null && d2 == null) return 0;
                if (d1 == null) return 1;
                if (d2 == null) return -1;

                try {
                    // FIXME: v2.3
                    Long l1 = Long.parseLong(d1);
                    Long l2 = Long.parseLong(d2);
                    result = l1.compareTo(l2);
                } catch (Throwable t) {
                    // FIXME: v2.4
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
                    try {
                        Date l1 = sdf.parse(d1);
                        Date l2 = sdf.parse(d2);
                        result = l1.compareTo(l2);
                    } catch (ParseException | IllegalArgumentException e) {
                        e.printStackTrace();
                    }
                }

                return result;
            }
        });
        notifyDataSetChanged();
    }

    public void updateProgress(UploadVisitsProgress uploadVisitsProgress) {
        String[] ids = uploadVisitsProgress.getVisitIds();
        for (String id : ids) {
            int position = getPosition(id);
            if (position != -1) {
                VisitAdapter.ViewHolder viewHolder = findViewHolderAtPosition(position);
                if (viewHolder != null) {
                    viewHolder.updateProgress(uploadVisitsProgress.getProgress(id));
                }
            }
        }
    }

    private int getPosition(String id) {
        for (int i = 0; i < mList.size(); i++) {
            if (mList.get(i).getId().equals(id)) return i;
        }
        return -1;
    }

    private VisitAdapter.ViewHolder findViewHolderAtPosition(int position) {
        return (VisitAdapter.ViewHolder) mRecyclerView.findViewHolderForAdapterPosition(position);
    }

    public interface Callback {
        void onClickItem(Visit visit);
    }
}
