package pro.mrvk.hiver.network.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import pro.mrvk.hiver.models.Config;

public class ConfigResDto extends BaseResDto {
    @Expose
    @SerializedName("data")
    private Config config;

    public Config getConfig() {
        return config;
    }
}
