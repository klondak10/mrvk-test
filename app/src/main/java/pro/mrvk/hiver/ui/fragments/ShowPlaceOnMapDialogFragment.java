package pro.mrvk.hiver.ui.fragments;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;

import pro.mrvk.hiver.R;
import pro.mrvk.hiver.activities.VisitActivity;

public class ShowPlaceOnMapDialogFragment extends AppCompatDialogFragment {

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        if (getActivity() != null) {
            final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            String[] dialog_fragment_array = getActivity().getResources().getStringArray(R.array.show_on_map_dialog_fragment_array);
            builder.setTitle(R.string.show_on_map_dialog_fragment_title)
                    .setItems(dialog_fragment_array, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            ((VisitActivity) getActivity()).showPlaceOnMapDialogFragment(i);
                        }
                    });
            return builder.create();
        } else {
            return super.onCreateDialog(savedInstanceState);
        }
    }
}
