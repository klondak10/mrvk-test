package pro.mrvk.hiver.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;

import pro.mrvk.hiver.BuildConfig;
import pro.mrvk.hiver.R;

import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.fabric.sdk.android.Fabric;
import pro.mrvk.hiver.Configuration;
import pro.mrvk.hiver.core.ICallback;
import pro.mrvk.hiver.services.LocationService;

import static pro.mrvk.hiver.core.PrefencesApi.KEY_AUTH_TOKEN;
import static pro.mrvk.hiver.core.PrefencesApi.KEY_MINIMUM_VERSION_CODE;
import static pro.mrvk.hiver.core.PrefencesApi.getPrefInteger;
import static pro.mrvk.hiver.core.PrefencesApi.getPrefString;

/**
 * Проверяем доступ к нужным сервисам и получаем конфигурацию для работы с приложением
 */
public class MainActivity extends AppCompatActivity {

    static private final String TAG = "MRVK_MainActv";

    @BindView(R.id.versionLabel)
    TextView versionLabel;
    @BindView(R.id.loadingProgressBar)
    ProgressBar loadingProgressBar;
    @BindView(R.id.permissInfoTextView)
    TextView permissInfoTextView;
    @BindView(R.id.permitSuccessButton)
    Button permitSuccessButton;

    private boolean isConfigReached = false;
    private boolean isPermissionGranted = false;
    private boolean isFirstPermissionCheccked = false;
    private boolean isPermissionRequested = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate");
        Fabric.with(this, new Crashlytics());

        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        versionLabel.setText("v" + BuildConfig.VERSION_NAME);

        if (BuildConfig.DEBUG) {
//            PrefencesApi.clearPref(KEY_AUTH_TOKEN);
        }

        try {
            Configuration.initConfig();
            fetchConfig();
            checkPermissions();
        } catch (Exception e) {
            e.printStackTrace();
            Crashlytics.logException(e);
        }
    }

    void fetchConfig() {
        final MainActivity me = this;
        // загружаем конфиг
        loadingProgressBar.setVisibility(View.VISIBLE);
        Configuration.updateConfiguration(new ICallback() {
            @Override
            public void callback() {
                isConfigReached = true;
                loadingProgressBar.setVisibility(View.GONE);
                // проверка минимальной версии приложения для работы
                try {
                    int minimumVersionCode = getPrefInteger(KEY_MINIMUM_VERSION_CODE);
                    if (BuildConfig.VERSION_CODE < minimumVersionCode) {
                        me.startActivity(new Intent(me, NeedUpdateActivity.class)
                                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                        finish();
                        return;
                    }
                    next();
                } catch (NullPointerException e) {
                    e.printStackTrace();
                    Crashlytics.logException(e);
                }
            }
        });
    }

    void checkPermissions() {
        if (Build.VERSION.SDK_INT < 23) {
            isPermissionGranted = true;
            next();
            return;
        }

        isPermissionGranted = checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                && checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                && checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED
                && checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                && checkSelfPermission(Manifest.permission.INTERNET) == PackageManager.PERMISSION_GRANTED;

        permissInfoTextView.setVisibility(isPermissionGranted ? View.GONE : View.VISIBLE);

        if (isPermissionGranted) {
            permitSuccessButton.setVisibility(View.GONE);
            next();
            return;
        }

        boolean isAnyGrantDenied = shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                || shouldShowRequestPermissionRationale(Manifest.permission.READ_EXTERNAL_STORAGE)
                || shouldShowRequestPermissionRationale(Manifest.permission.CAMERA)
                || shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_COARSE_LOCATION)
                || shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION)
                || shouldShowRequestPermissionRationale(Manifest.permission.INTERNET);

        permitSuccessButton.setVisibility(isFirstPermissionCheccked && isAnyGrantDenied || (!isAnyGrantDenied && isPermissionRequested)
                ? View.VISIBLE : View.GONE);

        if (!isPermissionRequested) {
            requestPermissions(new String[]{
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.CAMERA,
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.INTERNET,
            }, 3333);
            isPermissionRequested = true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        isFirstPermissionCheccked = true;
        next();
    }

    @OnClick(R.id.permitSuccessButton)
    public void PermitSuccessButtonClick() {
        Intent intent = new Intent();
        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getPackageName(), null);
        intent.setData(uri);
        startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        next();
    }

    void next() {
        if (!isPermissionGranted) {
            checkPermissions();
        } else {
            LocationService.start(this);
        }
        if (isPermissionGranted && isConfigReached) {
            gotoNextActivity();
        }
    }

    void gotoNextActivity() {
        try {
            String authToken = getPrefString(KEY_AUTH_TOKEN);
            if (authToken != null) {
                Crashlytics.log("Auto login with saved Auth Token :: " + authToken);
                Crashlytics.setString("Auto Auth Token", authToken);
                Crashlytics.setString("Auth Time", new Date().toString());
                startActivity(new Intent(this, PlacesActivity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
            } else {
                startActivity(new Intent(this, LoginActivity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
            }
            finish();
        } catch (Exception e) {
            e.printStackTrace();
            Crashlytics.logException(e);
        }
    }
}
