package pro.mrvk.hiver.models;

public class VisitDoubleValue extends VisitValue {

    public VisitDoubleValue(Double value) {
        type = DOUBLE_TYPE;
        doubleValue = value;
    }

    public Double getValue() {
        return doubleValue;
    }
}
