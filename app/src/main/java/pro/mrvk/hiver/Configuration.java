package pro.mrvk.hiver;

import com.crashlytics.android.Crashlytics;

import pro.mrvk.hiver.core.ICallback;
import pro.mrvk.hiver.models.Config;
import pro.mrvk.hiver.network.api.ConfigurationApi;

import static pro.mrvk.hiver.core.PrefencesApi.KEY_MAX_PHOTO_FILESIZE;
import static pro.mrvk.hiver.core.PrefencesApi.KEY_VISIT_POINT_RADIUS;
import static pro.mrvk.hiver.core.PrefencesApi.KEY_MINIMUM_VERSION_CODE;
import static pro.mrvk.hiver.core.PrefencesApi.prefContains;
import static pro.mrvk.hiver.core.PrefencesApi.setPrefValue;

public final class Configuration {
    private Configuration() {
    }

//    public static final String BACKEND_API_URL = "https://cabinet.mrvk.pro";
    public static final String BACKEND_API_URL = "https://mur21.d87.ru";
//    public static final String BACKEND_API_URL = "http://192.168.1.247:8800";

    public static final String URL_CONFIG = "/api/config";
    public static final String URL_LOGIN = "/api/login";
    public static final String URL_FETCH_VISITS = "/api/visits/route";
    public static final String URL_START_VISITS = "/api/visits/start";
    public static final String URL_REPORT_VISITS = "/api/visits/report";
    public static final String URL_UPLOAD_VISITS = "/api/visits/upload";
    public static final String URL_FIHISH_VISITS = "/api/visits/finish";
    public static final String URL_SEND_LOCATION_POINT = "/api/location/save";
    public static final String URL_SEND_LOG = "/api/log/save";

    /*
     * Инициализация конфигурации приложения
     */
    public static void initConfig() {
        // Радиус точки визита
        // При старте визита проверяется находится ли сотрудник в этом радиусе от точки
        if (!prefContains(KEY_VISIT_POINT_RADIUS)) {
            setPrefValue(KEY_VISIT_POINT_RADIUS, 500);
        }

        // Перед отправкой на сервер фотографии должны сжиматься до этого размера
        if (!prefContains(KEY_MAX_PHOTO_FILESIZE)) {
            setPrefValue(KEY_MAX_PHOTO_FILESIZE, 1_000_000);
        }
    }

    /*
     * API для обновления конфигурации с сервера
     */
    public static void updateConfiguration(final ICallback callback) {
        ConfigurationApi.getConfiguration(new ConfigurationApi.IConfigurationApiCallback() {
            @Override
            public void callback(Config config) {
                // сохраняем конфиг
                int visitPointRadius = config.getVisitPointRadius();
                int maxPhotoFilesize = config.getMaxPhotoFilesize();
                int minimumVersionCode = config.getMinimumVersionCode();
                setPrefValue(KEY_VISIT_POINT_RADIUS, visitPointRadius);
                setPrefValue(KEY_MAX_PHOTO_FILESIZE, maxPhotoFilesize);
                setPrefValue(KEY_MINIMUM_VERSION_CODE, minimumVersionCode);
                Crashlytics.setInt("Config.KEY_VISIT_POINT_RADIUS", visitPointRadius);
                Crashlytics.setInt("Config.KEY_MAX_PHOTO_FILESIZE", maxPhotoFilesize);
                Crashlytics.setInt("Config.KEY_MINIMUM_VERSION_CODE", minimumVersionCode);
                // возвращаем управление назад
                callback.callback();
            }
        });
    }
}
