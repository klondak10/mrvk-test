package pro.mrvk.hiver.network;

import pro.mrvk.hiver.network.dto.ConfigResDto;
import pro.mrvk.hiver.network.dto.LoginResDto;
import pro.mrvk.hiver.network.dto.LoginReqDto;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

import static pro.mrvk.hiver.Configuration.URL_CONFIG;
import static pro.mrvk.hiver.Configuration.URL_LOGIN;

public interface PublicRouteApi {
    @GET(URL_CONFIG)
    Call<ConfigResDto> getConfig();

    @POST(URL_LOGIN)
    Call<LoginResDto> postLogin(@Body LoginReqDto dto);
}
