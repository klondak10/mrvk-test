package pro.mrvk.hiver.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class VisitTemplateBlock {

    public static final String TYPE_DESCRIPTION = "DESCRIPTION";
    public static final String TYPE_PHOTOS = "PHOTOS";
    public static final String TYPE_CHECKERS = "CHECKERS";
    public static final String TYPE_NUMBERS = "NUMBERS";
    public static final String TYPE_COMMENTS = "COMMENTS";

    @Expose
    @SerializedName("id")
    private String id;

    @Expose
    @SerializedName("order")
    private int order;

    @Expose
    @SerializedName("type")
    private String type;

    @Expose
    @SerializedName("title")
    private String title;

    @Expose
    @SerializedName("required")
    private boolean required;

    @Expose
    @SerializedName("fields")
    private List<VisitTemplateBlockField> fields;

    public String getId() {
        return id;
    }

    public int getOrder() {
        return order;
    }

    public String getType() {
        return type;
    }

    public String getTitle() {
        return title;
    }

    public boolean isRequired() {
        return required;
    }

    public List<VisitTemplateBlockField> getFields() {
        return fields;
    }
}
