package pro.mrvk.hiver.network.api;

import com.crashlytics.android.Crashlytics;

import java.net.UnknownHostException;

import pro.mrvk.hiver.core.API;
import pro.mrvk.hiver.models.Config;
import pro.mrvk.hiver.network.dto.ConfigResDto;
import pro.mrvk.hiver.services.NetworkService;
import retrofit2.Call;
import retrofit2.Callback;

public final class ConfigurationApi {
    private ConfigurationApi() {
    }

    public interface IConfigurationApiCallback {
        void callback(Config config);
    }

    /*
     * API для обновления конфигурации с сервера
     */
    public static void getConfiguration(final IConfigurationApiCallback callback) {
        NetworkService.getInstance().getPublicApi()
                .getConfig()
                .enqueue(new Callback<ConfigResDto>() {
                    @Override
                    public void onResponse(Call<ConfigResDto> call, retrofit2.Response<ConfigResDto> response) {
                        ConfigResDto resDto = response.body();
                        if (resDto == null) {
                            API.showToast("Ошибка загрузки конфигурации: Ошибка на сервере", 5);
                            return;
                        }
                        if (resDto.getError() != null) {
                            API.showToast("Ошибка загрузки конфигурации: " + resDto.getError(), 5);
                            return;
                        }
                        Config config = resDto.getConfig();
                        if (config == null) {
                            API.showToast("Ошибка загрузки конфигурации: Сервер не вернул настройки", 5);
                            return;
                        }
                        callback.callback(config);
                    }

                    @Override
                    public void onFailure(Call<ConfigResDto> call, Throwable t) {
                        t.printStackTrace();
                        if (t instanceof UnknownHostException) {
                            // все нормально, нет сети
                            return;
                        }
                        Crashlytics.logException(t);
                        API.showToast("CRASH: Ошибка загрузки конфигурации", 5);
                    }
                });
    }
}
