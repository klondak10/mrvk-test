package pro.mrvk.hiver.network;

import java.util.Map;

import pro.mrvk.hiver.network.dto.LogReqDto;
import pro.mrvk.hiver.network.dto.LogResDto;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.HeaderMap;
import retrofit2.http.POST;

import static pro.mrvk.hiver.Configuration.URL_SEND_LOG;

public interface LogRouteApi {
    @POST(URL_SEND_LOG)
    Call<LogResDto> sendLog(
            @HeaderMap Map<String, String> headers,
            @Body LogReqDto dto);
}
