package pro.mrvk.hiver.helpers;

import android.util.Base64;

import java.security.KeyFactory;
import java.security.spec.EncodedKeySpec;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.Signature;

public class FormatNaviUrlHelper {

    // Формирует подпись с помощью ключа.
    public String sha256rsa(String key, String data) throws SecurityException {
        String trimmedKey = key.replaceAll("-----\\w+ PRIVATE KEY-----", "") .replaceAll("\\s", "");
        try {
            byte[] result = Base64.decode(trimmedKey, Base64.DEFAULT);
            KeyFactory factory = KeyFactory.getInstance("RSA", "BC");
            EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(result);
            Signature signature = Signature.getInstance("SHA256withRSA"); signature.initSign(factory.generatePrivate(keySpec));
            signature.update(data.getBytes());
            byte[] encrypted = signature.sign();
            return Base64.encodeToString(encrypted, Base64.NO_WRAP);
        } catch (Exception e) {
            throw new SecurityException("Error calculating cipher data. SIC!");
        }
    }
}
