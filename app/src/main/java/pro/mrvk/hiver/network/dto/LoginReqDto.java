package pro.mrvk.hiver.network.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginReqDto {
    @Expose
    @SerializedName("phone")
    private String phone;

    @Expose
    @SerializedName("login")
    private String login;

    @Expose
    @SerializedName("password")
    private String password;

    @Expose
    @SerializedName("deviceInfo")
    private String deviceInfo;

    public LoginReqDto(String phone, String login, String password, String deviceInfo) {
        this.phone = phone;
        this.login = login;
        this.password = password;
        this.deviceInfo = deviceInfo;
    }
}
