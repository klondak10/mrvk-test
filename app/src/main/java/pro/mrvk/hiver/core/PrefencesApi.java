package pro.mrvk.hiver.core;

import android.content.Context;
import android.content.SharedPreferences;

import pro.mrvk.hiver.app.App;

public class PrefencesApi {
    private static final String MY_SETTINGS = "MRVK_SETTINGS";

    private static SharedPreferences sharedPreferences = App.getContext()
            .getSharedPreferences(MY_SETTINGS, Context.MODE_PRIVATE);

    public static final String KEY_AUTH_TOKEN = "KEY_AUTH_TOKEN";
    public static final String KEY_VISIT_POINT_RADIUS = "KEY_VISIT_POINT_RADIUS";
    public static final String KEY_MAX_PHOTO_FILESIZE = "KEY_MAX_PHOTO_FILESIZE";
    public static final String KEY_MINIMUM_VERSION_CODE = "KEY_MINIMUM_VERSION_CODE";

    // close constructor
    private PrefencesApi() {}

    public static boolean prefContains(String key) {
        return sharedPreferences.contains(key);
    }

    public static void clearPref(String key) {
        sharedPreferences.edit().remove(key).apply();
    }

    public static String getPrefString(String key) {
        return sharedPreferences.getString(key, null);
    }

    public static Integer getPrefInteger(String key) {
        String value = sharedPreferences.getString(key, null);
        return value == null ? null : Integer.parseInt(value);
    }

    public static void setPrefValue(String key, String value) {
        sharedPreferences.edit().putString(key, value).apply();
    }

    public static void setPrefValue(String key, Integer value) {
        sharedPreferences.edit().putString(key, value == null ? null : value.toString()).apply();
    }
}
