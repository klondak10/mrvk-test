package pro.mrvk.hiver.network;

import java.util.Map;

import pro.mrvk.hiver.network.dto.LocationPointListReqDto;
import pro.mrvk.hiver.network.dto.LocationPointListResDto;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.HeaderMap;
import retrofit2.http.POST;

import static pro.mrvk.hiver.Configuration.URL_SEND_LOCATION_POINT;

public interface LocationRouteApi {
    @POST(URL_SEND_LOCATION_POINT)
    Call<LocationPointListResDto> sendLocationPoint(
            @HeaderMap Map<String, String> headers,
            @Body LocationPointListReqDto dto);
}
