package pro.mrvk.hiver.services;

import android.Manifest;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.crashlytics.android.Crashlytics;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Date;

import okhttp3.ResponseBody;
import pro.mrvk.hiver.BuildConfig;
import pro.mrvk.hiver.app.App;
import pro.mrvk.hiver.core.API;
import pro.mrvk.hiver.models.LocationPoint;
import pro.mrvk.hiver.models.LocationPointList;
import pro.mrvk.hiver.network.dto.LocationPointListResDto;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static pro.mrvk.hiver.core.PrefencesApi.KEY_AUTH_TOKEN;
import static pro.mrvk.hiver.core.PrefencesApi.clearPref;
import static pro.mrvk.hiver.core.PrefencesApi.getPrefString;

public class LocationService extends Service {

    static private final String TAG = "MRVK_LocationSrv";

    static private LocationManager locationManager;

    static private LocationListener passiveLocationListener;

    static private LocationListener networkLocationListener;

    static private LocationListener gpsLocationListener;

    // последняя обновленная точка
    // сначала загружаем из данных устройства
    // в процессе обновления точки, она перезаписывается
    static private LocationPoint lastLocationPoint = null;

    static public LocationPoint getLastLocationPoint() {
        return lastLocationPoint;
    }

    // очередь точек
    static private ArrayList<LocationPoint> locationPointsHistory = new ArrayList<>();

    // последняя сохраненная точка в истории
    // нужна для рассчета дистанции и дельты времени
    static private LocationPoint lastSavedHistoryPoint;

    // флаг отправления запроса на сервер
    static protected boolean isSaveHistoryRequesting = false;

    private class LocationListener implements android.location.LocationListener {

        private String locationServiceName;

        private String index;

        // переведется в false сразу же если провайдер отключен
        // это выполняется при вызове requestLocationUpdates и последующем onProviderDisabled
        private boolean enabled = true;

        LocationListener(String name) {
            locationServiceName = name;
            index = name + "_" + String.valueOf(Math.random()).substring(2, 5);
            Log.d(TAG, "LocList (" + index + ") constructed");
        }

        @Override
        public void onLocationChanged(Location newLocation) {
            try {
                // дальше считаю расстояния от предыдущей сохраненной точки
                double lat1 = newLocation.getLatitude();
                double lon1 = newLocation.getLongitude();
                Crashlytics.setString("Location " + locationServiceName, lat1 + ", " + lon1);

                Log.d(TAG, "LocList (" + index + ") onLocationChanged " + lat1 + ", " + lon1);

                long deltaTime = -1;
                double distance = -1;

                if (lastSavedHistoryPoint != null) {
                    deltaTime = new Date().getTime() - lastSavedHistoryPoint.getDate().getTime();
                    double lat2 = lastSavedHistoryPoint.getLatitude();
                    double lon2 = lastSavedHistoryPoint.getLongitude();
                    distance = API.calculateDistance(lat1, lon1, lat2, lon2);
                }

                // сохраняю текущую точку локации
                lastLocationPoint = new LocationPoint(newLocation, new Date(), deltaTime, locationServiceName, distance);

                if (lastSavedHistoryPoint == null || distance > 100 || deltaTime > (BuildConfig.DEBUG ? 30_000 : 300_000)) {
                    // предыдущей точки не было,
                    // либо устройство сместилось достаточно далеко от предыдущей точки,
                    // либо прошло достаточно времени с прошлого обновления точки,
                    // будем сохранять текущую точку в базу
                    Log.d(TAG, "LocList (" + index + ") add point " + locationServiceName + " : " + deltaTime + " ms; " + lat1 + ", " + lon1 + "; distance=" + distance);
                    locationPointsHistory.add(lastLocationPoint);
                    lastSavedHistoryPoint = lastLocationPoint;
                    updateEmployeePosition();
                }
            } catch (Exception e) {
                Log.d(TAG, "LocList (" + index + ") onLocationChanged ERROR: ", e);
            }
        }

        /**
         * This method was deprecated in API level 29. This callback will never be invoked.
         * https://developer.android.com/reference/android/location/LocationListener
         */
        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
//            Log.d(TAG, "LocList (" + index + ") onStatusChanged: " + provider + ", status = " + status);
        }

        /**
         * Евент случается только в момент включения GPS датчика.
         * Либо при включении опции в настройках "Доступ к местоположению".
         */
        @Override
        public void onProviderEnabled(String provider) {
            Log.d(TAG, "LocList (" + index + ") onProviderEnabled: " + provider);
            enabled = true;
        }

        /**
         * Евент случается только в момент отключения GPS датчика.
         * Либо при отключении опции в настройках "Доступ к местоположению".
         */
        @Override
        public void onProviderDisabled(String provider) {
            Log.d(TAG, "LocList (" + index + ") onProviderDisabled: " + provider);
            enabled = false;
        }
    }

    /**
     * Отправка истории точек на сервер
     */
    static protected void updateEmployeePosition() {
        if (isSaveHistoryRequesting) {
            return;
        }

        // будем отправлять только если в истории не менее пяти точек
        if (locationPointsHistory.size() < (BuildConfig.DEBUG ? 1 : 5)) {
            return;
        }

        // история отправляется только для авторизованных пользователей
        if (getPrefString(KEY_AUTH_TOKEN) == null) {
            return;
        }

        isSaveHistoryRequesting = true;

        final int sendedCount = locationPointsHistory.size();
        LocationPointList locationPoints = new LocationPointList(null);
        for (int i = 0; i < sendedCount; i++) {
            locationPoints.add(locationPointsHistory.get(i));
        }

        NetworkService.getInstance().sendLocationPoint(locationPoints)
                .enqueue(new Callback<LocationPointListResDto>() {
                    @Override
                    public void onResponse(Call<LocationPointListResDto> call, Response<LocationPointListResDto> response) {
                        isSaveHistoryRequesting = false;
                        if (response.code() == 403) {
                            clearPref(KEY_AUTH_TOKEN);
                            API.showToast("Ошибка авторизации при отправке позиции");
                            // FIXME тут обнаружили, что токен не подходит, нужно выкинуться на LoginActivity
                            return;
                        }
                        if (BuildConfig.DEBUG) {
                            try {
                                LocationPointListResDto body = response.body();
                                ResponseBody errorBody = response.errorBody();
                                String content = (body != null) ? "Body: error = " + body.getError() + ", error_const = " + body.getErrorConst()
                                        : (errorBody != null) ? "ErrorBody: " + errorBody.string()
                                        : "not body";
                                System.out.println("------ [Response] status: " + response.code() + "\n" + content + "\n----------------\n");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        // очистка данных, которые уже отправили
                        ArrayList<LocationPoint> newHistory = new ArrayList<>();
                        for (int i = sendedCount; i < locationPointsHistory.size(); i++) {
                            newHistory.add(locationPointsHistory.get(i));
                        }
                        locationPointsHistory = newHistory;
                    }

                    @Override
                    public void onFailure(Call<LocationPointListResDto> call, Throwable t) {
                        isSaveHistoryRequesting = false;
                        t.printStackTrace();
                        if (t instanceof UnknownHostException) {
                            // все нормально, нет сети
                            return;
                        }
                        Crashlytics.logException(t);
                        API.showToast("CRASH: Ошибка отправки позиции");
                    }
                });
    }

    /**
     * Метод для запуска сервиса
     *
     * @param context
     */
    static public void start(Context context) {

        Log.d(TAG, "start");
        try {
            context.startService(new Intent(context, LocationService.class));
        } catch (Exception e) {
            e.printStackTrace();
            Crashlytics.logException(e);
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Log.d(TAG, "onBind");
        return null;
    }

    @Override
    public void onCreate() {
        Log.d(TAG, "onCreate: LocationService");
    }

    @Override
    public void onLowMemory() {
        Log.d(TAG, "onLowMemory");
        super.onLowMemory();
    }

    @Override
    public boolean onUnbind(Intent intent) {
        Log.d(TAG, "onUnbind");
        //return super.onUnbind(intent);
        return false;
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "onDestroy: LocationService");
        super.onDestroy();

        // restart myself
        start(App.getContext());
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand: flags = " + flags + ", startId = " + startId);
        startListeners();
        return START_STICKY; // START_REDELIVER_INTENT;
    }

    void startListeners() {
        Log.d(TAG, "startListeners: locationManager = " + locationManager);

        if (locationManager != null) {
            stopListeners();
        }

        // setup Location Manager
        locationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        // получаем последнюю позицию устройства
        try {
            Location lastLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if (lastLocation == null) {
                lastLocation = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            }
            if (lastLocation == null) {
                lastLocation = locationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
            }
            if (lastLocation != null) {
                lastLocationPoint = new LocationPoint(lastLocation, new Date(), -1, "HARDWARE SAVED LOCATION POINT", -1);
            }
        } catch (NullPointerException | SecurityException e) {
            e.printStackTrace();
            Crashlytics.logException(e);
        }

        // подписываемся на обновление
        try {
            int LOCATION_INTERVAL = BuildConfig.DEBUG ? 5000 : 10000; // ms
            float LOCATION_DISTANCE = BuildConfig.DEBUG ? 1.0f : 10f; // metres
            // create
            passiveLocationListener = new LocationListener("Passive");
            networkLocationListener = new LocationListener("Network");
            gpsLocationListener = new LocationListener("GPS");
            // connect
            locationManager.requestLocationUpdates(LocationManager.PASSIVE_PROVIDER, LOCATION_INTERVAL, LOCATION_DISTANCE, passiveLocationListener);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, LOCATION_INTERVAL, LOCATION_DISTANCE, networkLocationListener);
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, LOCATION_INTERVAL, LOCATION_DISTANCE, gpsLocationListener);
        } catch (SecurityException e) {
            e.printStackTrace();
            Crashlytics.logException(e);
        }

        if (Build.VERSION.SDK_INT >= 24) {
            // стартуем с иконкой
            /*
            Intent notificationIntent = new Intent(this, LocationService.class);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);
            Notification notification = new Notification(getApplicationContext(),
                    R.drawable.icon_add_photo,
                    getText(R.string.project_id),
                    System.currentTimeMillis(),
                    getText(R.string.project_id),
                    getText(R.string.project_id),
                    notificationIntent);

//                notification.setLatestEventInfo(this, getText(R.string.google_app_id), getText(R.string.project_id), pendingIntent);
            startForeground(0x333, notification);
            */
        }
    }

    void stopListeners() {
        Log.d(TAG, "stopListeners: locationManager = " + locationManager);
        final int GRANTED = PackageManager.PERMISSION_GRANTED;
        if (locationManager != null) {
            if (Build.VERSION.SDK_INT >= 24) {
//                stopForeground(STOP_FOREGROUND_REMOVE);
            }

            try {
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != GRANTED &&
                        ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != GRANTED) {
                    return;
                }
                locationManager.removeUpdates(gpsLocationListener);
                locationManager.removeUpdates(networkLocationListener);
                locationManager.removeUpdates(passiveLocationListener);
                // clear
                passiveLocationListener = null;
                networkLocationListener = null;
                gpsLocationListener = null;
            } catch (Exception e) {
                e.printStackTrace();
                Crashlytics.logException(e);
            }
            locationManager = null;
        }
    }
}
