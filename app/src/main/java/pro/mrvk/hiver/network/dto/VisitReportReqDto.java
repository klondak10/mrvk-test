package pro.mrvk.hiver.network.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import pro.mrvk.hiver.models.Visit;
import pro.mrvk.hiver.models.VisitTemplateBlock;
import pro.mrvk.hiver.models.VisitTemplateBlockField;

public class VisitReportReqDto {
    @Expose
    @SerializedName("templateId")
    private String templateId;

    @Expose
    @SerializedName("templateVersion")
    private String templateVersion;

    @Expose
    @SerializedName("visitId")
    private String visitId;

    @Expose
    @SerializedName("placeId")
    private String placeId;

    @Expose
    @SerializedName("startTime")
    private String startTime;

    @Expose
    @SerializedName("finishTime")
    private String finishTime;

    @Expose
    @SerializedName("startPosition")
    private String startPosition;

    @Expose
    @SerializedName("finishPosition")
    private String finishPosition;

    @Expose
    @SerializedName("values")
    private List<VisitReportValueDto> values;

    private VisitReportReqDto() {
    }

    static public VisitReportReqDto of(Visit visit) {
        VisitReportReqDto dto = new VisitReportReqDto();
        dto.templateId = visit.getTemplate().getId();
        dto.templateVersion = visit.getTemplate().getVersion();
        dto.visitId = visit.getId();
        dto.placeId = visit.getPlace().getId();

        dto.startTime = visit.getStartTime();
        dto.finishTime = visit.getFinishTime();
        dto.startPosition = visit.getStartPosition();
        dto.finishPosition = visit.getFinishPosition();
        dto.values = new ArrayList<>();
        for (VisitTemplateBlock block : visit.getTemplate().getBlocks()) {
            if (block.getType().equals(VisitTemplateBlock.TYPE_DESCRIPTION)
                    || block.getType().equals(VisitTemplateBlock.TYPE_PHOTOS))
                continue;
            List<VisitReportValueFieldDto> fields = new ArrayList<>();
            for (VisitTemplateBlockField field : block.getFields()) {
                String fieldId = field.getId();
                Object value = null;
                switch (block.getType()) {
                    case VisitTemplateBlock.TYPE_COMMENTS:
                        value = visit.getStringValue(fieldId);
                        break;
                    case VisitTemplateBlock.TYPE_NUMBERS:
                        value = visit.getDoubleValue(fieldId);
                        break;
                    case VisitTemplateBlock.TYPE_CHECKERS:
                        value = visit.getBooleanValue(fieldId);
                        if (value == null) {
                            value = false;
                        }
                        break;
                }
                if (value != null)
                    fields.add(new VisitReportValueFieldDto(fieldId, value));
            }
            if (fields.size() > 0) {
                VisitReportValueDto value = new VisitReportValueDto(block.getId(), fields);
                dto.values.add(value);
            }
        }
        return dto;
    }
}
