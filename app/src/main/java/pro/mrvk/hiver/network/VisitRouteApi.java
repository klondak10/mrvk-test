package pro.mrvk.hiver.network;

import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import pro.mrvk.hiver.network.dto.VisitFinishReqDto;
import pro.mrvk.hiver.network.dto.VisitFinishResDto;
import pro.mrvk.hiver.network.dto.VisitReportReqDto;
import pro.mrvk.hiver.network.dto.VisitReportResDto;
import pro.mrvk.hiver.network.dto.VisitRouteResDto;
import pro.mrvk.hiver.network.dto.VisitStartReqDto;
import pro.mrvk.hiver.network.dto.VisitStartResDto;
import pro.mrvk.hiver.network.dto.VisitUploadResDto;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.HeaderMap;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

import static pro.mrvk.hiver.Configuration.URL_FETCH_VISITS;
import static pro.mrvk.hiver.Configuration.URL_FIHISH_VISITS;
import static pro.mrvk.hiver.Configuration.URL_REPORT_VISITS;
import static pro.mrvk.hiver.Configuration.URL_START_VISITS;
import static pro.mrvk.hiver.Configuration.URL_UPLOAD_VISITS;

public interface VisitRouteApi {
    @GET(URL_FETCH_VISITS)
    Call<VisitRouteResDto> getVisitRoute(
            @HeaderMap Map<String, String> headers,
            @Query("date") String date);

    @POST(URL_START_VISITS)
    Call<VisitStartResDto> postVisitStart(
            @HeaderMap Map<String, String> headers,
            @Body VisitStartReqDto dto);

    @POST(URL_REPORT_VISITS)
    Call<VisitReportResDto> postVisitReport(
            @HeaderMap Map<String, String> headers,
            @Body VisitReportReqDto dto);

    @Multipart
    @POST(URL_UPLOAD_VISITS)
    Call<VisitUploadResDto> postVisitUpload(
            @HeaderMap Map<String, String> headers,
            @Part("visitId") RequestBody visitId,
            @Part("blockId") RequestBody blockId,
            @Part("fieldId") RequestBody fieldId,
            @Part MultipartBody.Part file);

    @POST(URL_FIHISH_VISITS)
    Call<VisitFinishResDto> postVisitFinish(
            @HeaderMap Map<String, String> headers,
            @Body VisitFinishReqDto dto);
}
