package pro.mrvk.hiver.network.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import pro.mrvk.hiver.models.Visit;

public class VisitRouteResDto extends BaseResDto {
    @Expose
    @SerializedName("data")
    private List<Visit> visits;

    public List<Visit> getVisits() {
        return visits;
    }
}
