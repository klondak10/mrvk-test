package pro.mrvk.hiver.network.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VisitReportValueFieldDto {
    @Expose
    @SerializedName("id")
    private String id;

    @Expose
    @SerializedName("value")
    private Object value;

    public VisitReportValueFieldDto(String id, Object value) {
        this.id = id;
        this.value = value;
    }
}
