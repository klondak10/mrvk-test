package pro.mrvk.hiver.app;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;

import com.bumptech.glide.request.target.ViewTarget;
import pro.mrvk.hiver.R;
import pro.mrvk.hiver.helpers.PhotoManager;

public class App extends Application {

    @SuppressLint("StaticFieldLeak")
    private static Context mContext;

    @Override
    public void onCreate() {
        super.onCreate();
        setContext(getApplicationContext());

        PhotoManager.get().bind();

        ViewTarget.setTagId(R.id.glide_tag);
    }

    private static void setContext(Context context) {
        mContext = context;
    }

    public static Context getContext() {
        return mContext;
    }
}
