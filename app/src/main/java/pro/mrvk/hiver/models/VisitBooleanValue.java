package pro.mrvk.hiver.models;

public class VisitBooleanValue extends VisitValue {

    public VisitBooleanValue(Boolean value) {
        type = BOOLEAN_TYPE;
        booleanValue = value;
    }

    public Boolean getValue() {
        return booleanValue;
    }
}
