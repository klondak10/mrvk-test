package pro.mrvk.hiver.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Config {

    @Expose
    @SerializedName("visit_point_radius")
    private int visitPointRadius;

    @Expose
    @SerializedName("max_photo_filesize")
    private int maxPhotoFilesize;

    @Expose
    @SerializedName("minimum_version_code")
    private int minimumVersionCode;

    public int getVisitPointRadius() {
        return visitPointRadius;
    }

    public int getMaxPhotoFilesize() {
        return maxPhotoFilesize;
    }

    public int getMinimumVersionCode() {
        return minimumVersionCode;
    }
}
