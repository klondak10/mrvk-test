package pro.mrvk.hiver.models;

import android.widget.TextView;

import java.util.List;

public class TitleModel {
    private TextView title;
    private String viewId;
    private List<String> viewIds;

    public TitleModel(TextView title, List<String> viewIds) {
        this.title = title;
        this.viewIds = viewIds;
    }

    public List<String> getViewIds() {
        return viewIds;
    }

    public TitleModel(TextView title, String viewId) {
        this.title = title;
        this.viewId = viewId;
    }

    public TextView getTitle() {
        return title;
    }

    public void setTitle(TextView title) {
        this.title = title;
    }

    public String getViewId() {
        return viewId;
    }

    public void setViewId(String viewId) {
        this.viewId = viewId;
    }
}
