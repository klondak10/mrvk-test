package pro.mrvk.hiver.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.LayoutRes;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.crashlytics.android.Crashlytics;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import butterknife.BindColor;
import io.fabric.sdk.android.Fabric;
import pro.mrvk.hiver.BuildConfig;
import pro.mrvk.hiver.R;

import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEvent;
import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEventListener;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindDrawable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pro.mrvk.hiver.models.ApplicationMapInfo;
import pro.mrvk.hiver.core.API;
import pro.mrvk.hiver.eventbus.SavedVisitEvent;
import pro.mrvk.hiver.helpers.DateHelper;
import pro.mrvk.hiver.helpers.FormatNaviUrlHelper;
import pro.mrvk.hiver.helpers.ImageHelper;
import pro.mrvk.hiver.models.LocationPoint;
import pro.mrvk.hiver.models.TitleModel;
import pro.mrvk.hiver.models.Visit;
import pro.mrvk.hiver.models.VisitBooleanValue;
import pro.mrvk.hiver.models.VisitDoubleValue;
import pro.mrvk.hiver.models.VisitPhotoValue;
import pro.mrvk.hiver.models.VisitPlace;
import pro.mrvk.hiver.models.VisitStatus;
import pro.mrvk.hiver.models.VisitStringValue;
import pro.mrvk.hiver.models.VisitTemplateBlock;
import pro.mrvk.hiver.models.VisitTemplateBlockField;
import pro.mrvk.hiver.models.VisitValue;
import pro.mrvk.hiver.network.dto.VisitStartResDto;
import pro.mrvk.hiver.services.DatabaseService;
import pro.mrvk.hiver.services.LocationService;
import pro.mrvk.hiver.services.NetworkService;
import pro.mrvk.hiver.ui.adapters.AppInfoAdapter;
import pro.mrvk.hiver.ui.fragments.ShowPlaceOnMapDialogFragment;
import retrofit2.Call;
import retrofit2.Callback;

import static pro.mrvk.hiver.core.PrefencesApi.KEY_VISIT_POINT_RADIUS;
import static pro.mrvk.hiver.core.PrefencesApi.getPrefInteger;

public class VisitActivity extends AppCompatActivity {

    static private final String TAG = "MRVK_VisitActv";

    private static final int CAMERA_RC = 10;

    @BindView(R.id.blockView)
    LinearLayout blockView;
    @BindView(R.id.btnStartVisit)
    Button btnStart;
    @BindView(R.id.bottomSheet)
    View bottomSheet;
    @BindDrawable(R.drawable.circle_back_grn_left)
    Drawable colorGreen;
    @BindView(R.id.nsContainer)
    NestedScrollView nsContainer;

    private BottomSheetBehavior mBehavior;
    private BottomSheetDialog mBottomSheetDialog;

    private Visit visit;

    private String currentPhotoPath;
    private ImageView currentPhotoView;
    private List<View> viewsEditing = new ArrayList<>();
    private EditText lastEditText;

    private String currentIdViewPhoto = "";

    private List<TitleModel> viewTitleList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate");
        Fabric.with(this, new Crashlytics());

        setContentView(R.layout.activity_visit);
        ButterKnife.bind(this);

        setupVisitData();
        setupToolbar();
        setupButtonStart();
        createViews();

        mBehavior = BottomSheetBehavior.from(bottomSheet);
    }

    @Override
    protected void onStart() {
        super.onStart();
        LocationService.start(this);
    }

    @Override
    protected void onStop() {
        lastEtClearFocus();
        super.onStop();
    }

    @Override
    public void onBackPressed() {
        if (visit.getStatus().equals(VisitStatus.IN_PROGRESS)) {
            API.showToast(getString(R.string.must_complete_visit));
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.visit_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        } else if (item.getItemId() == R.id.action_place) {
            if (!appYandexnaviInstalled()) {
                showPlaceOnMap();
            } else {
                FragmentManager fragmentManager = getSupportFragmentManager();
                ShowPlaceOnMapDialogFragment showPlaceOnMapDialogFragment = new ShowPlaceOnMapDialogFragment();
                showPlaceOnMapDialogFragment.show(fragmentManager, "show_dialog");
            }
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == CAMERA_RC) {
                File photoFile = new File(currentPhotoPath);
                ImageHelper.compress(this, photoFile);

                String tag = (String) currentPhotoView.getTag();
                updateVisitValue(tag, new VisitPhotoValue(currentPhotoPath, false));
                showPhoto(currentPhotoView, currentPhotoPath, blockView.findViewWithTag(tag + "-delete"), currentIdViewPhoto);

                if (BuildConfig.DEBUG) {
                    FrameLayout parent1 = (FrameLayout) currentPhotoView.getParent();
                    LinearLayout parent2 = (LinearLayout) parent1.getParent();
                    for (int i = 0; i < parent2.getChildCount(); i++) {
                        FrameLayout frame = (FrameLayout) parent2.getChildAt(i);
                        if (frame == parent1) continue;
                        AppCompatImageView image = (AppCompatImageView) frame.getChildAt(0);
                        String tag1 = (String) image.getTag();
                        if (!visit.getValues().containsKey(tag1) || visit.getValues().get(tag1) == null) {
                            String newName = currentPhotoPath + "-" + i + ".jpg";
                            try {
                                API.copyFile(currentPhotoPath, newName);
                            } catch (IOException e) {
                                e.printStackTrace();
                                API.showToast("DEBUG: не удалось скопировать файл " + currentPhotoPath + " в " + newName, 5);
                            }
                            updateVisitValue(tag1, new VisitPhotoValue(newName, false));
                            showPhoto(image, currentPhotoPath, blockView.findViewWithTag(tag1 + "-delete"), currentIdViewPhoto);
                        }
                    }
                }
            }
        }
    }

    private boolean appYandexnaviInstalled() {
        PackageManager pm = getPackageManager();
        try {
            pm.getPackageInfo("ru.yandex.yandexnavi", PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    public void showPlaceOnMapDialogFragment(int result) {
        switch (result) {
            case 0:
                showPlaceOnYandexNavi();
                break;
            case 1:
                showPlaceOnMapNoNaviYandex();
                break;
            default:
                showPlaceOnMap();
                break;
        }
    }

    private void showPlaceOnYandexNavi() {
        try {
            String privateKey = getResources().getString(R.string.private_key_yandex);
            String locationPlace = visit.getPlace().getLocation();
            String[] latLongArray = locationPlace.split(",");
            FormatNaviUrlHelper formatNaviUrlHelper = new FormatNaviUrlHelper();
            Uri uri = Uri.parse("yandexnavi://build_route_on_map").buildUpon()
                    .appendQueryParameter("lat_to", latLongArray[0])
                    .appendQueryParameter("lon_to", latLongArray[1])
                    .appendQueryParameter("client", "154").build();
            String signature = formatNaviUrlHelper.sha256rsa(privateKey, uri.toString());
            uri = uri.buildUpon()
                    .appendQueryParameter("signature", signature)
                    .build();
            Intent mapIntent = new Intent(Intent.ACTION_VIEW, uri);
            mapIntent.setPackage("ru.yandex.yandexnavi");
            if (mapIntent.resolveActivity(getPackageManager()) != null) {
                startActivity(mapIntent);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Crashlytics.logException(e);
        }
    }

    private void showPlaceOnMapNoNaviYandex() {
        try {
            String geoString = "geo:%1$s?q=%1$s&z=16";
            String geoUriString = String.format(geoString, visit.getPlace().getLocation());
            Uri geoUri = Uri.parse(geoUriString);
            showBottomSheetDialog("ru.yandex.yandexnavi", geoUri);
        } catch (Exception e) {
            e.printStackTrace();
            Crashlytics.logException(e);
        }
    }

    private void showPlaceOnMap() {
        try {
            String geoString = "geo:%1$s?q=%1$s&z=16";
            String geoUriString = String.format(geoString, visit.getPlace().getLocation());
            Uri geoUri = Uri.parse(geoUriString);
            Intent mapIntent = new Intent(Intent.ACTION_VIEW, geoUri);
            if (mapIntent.resolveActivity(getPackageManager()) != null) {
                startActivity(mapIntent);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Crashlytics.logException(e);
        }
    }

    private List<ApplicationMapInfo> mapExludingApp(PackageManager packageManager, Context context, String packageNameToExclude, Uri geoUri) {
        List<ApplicationMapInfo> targetLists = new ArrayList<>();
        Intent map = new Intent(Intent.ACTION_VIEW, geoUri);
        List<ResolveInfo> resolveInfos = context.getPackageManager().queryIntentActivities(map, 0);
        if (!resolveInfos.isEmpty()) {
            for (ResolveInfo info : resolveInfos) {
                Intent geo = new Intent(Intent.ACTION_VIEW, geoUri);
                if (!info.activityInfo.packageName.equalsIgnoreCase(packageNameToExclude)) {
                    geo.setPackage(info.activityInfo.packageName);
                    ApplicationMapInfo appMapInfo = new ApplicationMapInfo();
                    appMapInfo.setIntent(geo);
                    appMapInfo.setIconDrawable(info.activityInfo.loadIcon(packageManager));
                    appMapInfo.setLabel(info.loadLabel(packageManager).toString());
                    targetLists.add(appMapInfo);
                }
            }

            return targetLists;
        }
        return null;
    }

    private void showBottomSheetDialog(String packageNameToExclude, Uri geoUri) {
        final List<ApplicationMapInfo> mapIntent = mapExludingApp(getPackageManager(), this, "ru.yandex.yandexnavi", geoUri);
        if (mapIntent != null) {
            if (mapIntent.size() > 1) {
                if (mBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
                    mBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                }

                final View view = getLayoutInflater().inflate(R.layout.bottom_sheet_list, null);

                RecyclerView listView = view.findViewById(R.id.listApp);

                AppInfoAdapter appInfoAdapter = new AppInfoAdapter(mapIntent);

                listView.setAdapter(appInfoAdapter);

                GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 4);

                listView.setLayoutManager(gridLayoutManager);

                appInfoAdapter.setOnClickListener(new AppInfoAdapter.OnClickListener() {
                    @Override
                    public void onItemClick(View view, int pos) {
                        Intent intent = mapIntent.get(pos).getIntent();
                        if (intent != null) {
                            if (intent.resolveActivity(getPackageManager()) != null) {
                                startActivity(intent);
                                mBottomSheetDialog.dismiss();
                            }
                        }
                    }
                });

                mBottomSheetDialog = new BottomSheetDialog(this);
                mBottomSheetDialog.setContentView(view);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    mBottomSheetDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                }

                mBottomSheetDialog.show();
                mBottomSheetDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        mBottomSheetDialog = null;
                    }
                });
            } else {
                Intent intent = mapIntent.get(0).getIntent();
                if (intent != null) {
                    if (intent.resolveActivity(getPackageManager()) != null) {
                        startActivity(intent);
                    }
                }
            }
        }
    }

    private void registerKeyboardVisibilityEvent() {
        KeyboardVisibilityEvent.setEventListener(this, new KeyboardVisibilityEventListener() {
            @Override
            public void onVisibilityChanged(boolean isOpen) {
                if (!isOpen) {
                    lastEtClearFocus();
                }
            }
        });
    }

    private void setupToolbar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);

            VisitPlace place = visit.getPlace();
            actionBar.setTitle(place.getAddress());
            actionBar.setSubtitle(place.getName() + ", " + place.getOrgName());
        }
    }

    private void setupVisitData() {
        visit = new Gson().fromJson(getIntent().getStringExtra("visit"), Visit.class);
        visit.initValues();
    }

    private void updateVisitValue(String key, VisitValue value) {
        if (value == null) {
            if (visit.getValues().containsKey(key)) {
                visit.getValues().remove(key);
                DatabaseService.updateVisitValues(visit);
            }
        } else {
            visit.getValues().put(key, value);
            DatabaseService.updateVisitValues(visit);
        }
    }

    private boolean isAllowEditing() {
        return !visit.getStatus().equals(VisitStatus.TODO);
    }

    private void addViewsEditing(View view) {
        viewsEditing.add(view);
    }

    private void allowEditing() {
        for (View view : viewsEditing) {
            view.setEnabled(true);
            view.setVisibility(View.VISIBLE);
        }
    }

    private View inflateView(@LayoutRes int layoutResId) {
        return View.inflate(this, layoutResId, null);
    }

    private void enableOrDisableView(View view) {
        view.setEnabled(isAllowEditing());
    }

    private void showOrHideView(View view, boolean show) {
        view.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    private void setupButtonStart() {
        if (!visit.getStatus().equals(VisitStatus.TODO)) {
            btnStart.setText(R.string.stop_visit);
            btnStart.setBackground(colorGreen);
        }
    }

    private void createViews() {
        for (VisitTemplateBlock block : visit.getTemplate().getBlocks()) {
            switch (block.getType()) {
                case VisitTemplateBlock.TYPE_DESCRIPTION:
                    createDescriptionBlock(block);
                    break;
                case VisitTemplateBlock.TYPE_PHOTOS:
                    createPhotosBlock(block);
                    break;
                case VisitTemplateBlock.TYPE_CHECKERS:
                    createCheckersBlock(block);
                    break;
                case VisitTemplateBlock.TYPE_NUMBERS:
                    createNumbersBlock(block);
                    break;
                case VisitTemplateBlock.TYPE_COMMENTS:
                    createCommentsBlock(block);
                    break;
            }
        }
        registerKeyboardVisibilityEvent();
    }

    private TextView createTitle(String text, boolean required) {
        TextView view = (TextView) inflateView(R.layout.visit_view_title);
        String coloredText = "<font color=#949494>" + text + "</font>";
        if (required) {
            coloredText += "<font color=#000000> *</font>";
        }
        view.setText(Html.fromHtml(coloredText));
        return view;
    }

    private LinearLayout createDescription(VisitTemplateBlockField field) {
        LinearLayout view = (LinearLayout) inflateView(R.layout.visit_view_description);
        TextView title = view.findViewById(R.id.title);
        title.setText(field.getTitle());
        TextView value = view.findViewById(R.id.value);
        value.setText(field.getValue());
        return view;
    }

    private FrameLayout createPhoto(final String id) {
        FrameLayout view = (FrameLayout) inflateView(R.layout.visit_view_photo);

        final ImageView photo = view.findViewById(R.id.photo);
        photo.setTag(id);
        photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                currentPhotoView = photo;
                currentIdViewPhoto = id;
                takePhotoFromCamera();
                updateVisitValue(id, new VisitPhotoValue(currentPhotoPath));
            }
        });

        final ImageButton delete = view.findViewById(R.id.delete);
        delete.setTag(id + "-delete");
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (visit.getValues().containsKey(id)) {
                    String path = visit.getPhotoValue(id).getPathName();
                    if (path != null) {
                        new File(path).delete();
                    }
                }
                photo.setImageResource(R.drawable.icon_add_photo);
                showOrHideView(delete, false);
                updateVisitValue(id, null);
            }
        });

        if (visit.getValues().containsKey(id)) {
            String path = visit.getPhotoValue(id).getPathName();
            if (path != null) {
                showPhoto(photo, path, delete, id);
            }
        }

        enableOrDisableView(photo);
        addViewsEditing(photo);
        return view;
    }

    private CheckBox createCheckbox(final String id, String text) {
        CheckBox view = (CheckBox) inflateView(R.layout.visit_view_check_box);
        view.setTag(id);
        view.setText(text);
        if (visit.getValues().containsKey(id)) {
            view.setChecked(visit.getBooleanValue(id));
        }
        view.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (!isChecked) {
                    updateVisitValue((String) buttonView.getTag(), new VisitBooleanValue(isChecked));

                } else {
                    for (TitleModel titleModel : viewTitleList) {
                        if (titleModel.getViewIds() != null && !titleModel.getViewIds().isEmpty()) {
                            for (String idBlock : titleModel.getViewIds()) {
                                if (id.equals(idBlock) && titleModel.getTitle().getCurrentTextColor()
                                        == ContextCompat.getColor(getBaseContext(), R.color.colorException)) {
                                    titleModel.getTitle().setText(titleModel.getTitle().getText().toString());
                                    titleModel.getTitle().setTextColor(ContextCompat.getColor(getBaseContext(), R.color.grayColor));
                                }
                            }
                        }
                    }
                    updateVisitValue((String) buttonView.getTag(), new VisitBooleanValue(isChecked));
                }
            }
        });

        enableOrDisableView(view);
        addViewsEditing(view);
        return view;
    }

    private LinearLayout createNumber(final VisitTemplateBlockField field) {
        final String fieldId = field.getId();
        LinearLayout view = (LinearLayout) inflateView(R.layout.visit_view_number);
        final EditText et = view.findViewById(R.id.et);
        et.setTag(fieldId);
        if (visit.getValues().containsKey(fieldId)) {
            Double number = visit.getDoubleValue(fieldId);
            if (number != null) {
                et.setText(String.valueOf(number));
            }
        }
        et.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                lastEditText = et;
                if (!hasFocus) {
                    try {
                        Double number = Double.parseDouble(et.getText().toString());
                        updateVisitValue(fieldId, new VisitDoubleValue(number));
                    } catch (NumberFormatException e) {
                        et.setText("");
                        updateVisitValue(fieldId, null);
                    }
                }
            }
        });
        et.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                et.removeTextChangedListener(this);
                for (TitleModel titleModel : viewTitleList) {
                    if (field.getId().equals(titleModel.getViewId()) && titleModel.getTitle().getCurrentTextColor()
                            == ContextCompat.getColor(getBaseContext(), R.color.colorException)) {
                        titleModel.getTitle().setText(titleModel.getTitle().getText().toString());
                        titleModel.getTitle().setTextColor(ContextCompat.getColor(getBaseContext(), R.color.grayColor));
                    }
                }
                switch (s.toString()) {
                    case ".":
                        et.setText("0.");
                        break;
                    case "-.":
                        et.setText("-");
                        break;
                    case "+":
                        et.setText("");
                        break;
                    case "00":
                        et.setText("0");
                        break;
                }
                et.setSelection(et.length());

                et.addTextChangedListener(this);
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });
        enableOrDisableView(et);
        addViewsEditing(et);

        TextView tv = view.findViewById(R.id.tv);
        tv.setText(field.getTitle());
        return view;
    }

    private LinearLayout createComment(final VisitTemplateBlockField field) {
        final String fieldId = field.getId();
        LinearLayout view = (LinearLayout) inflateView(R.layout.visit_view_comment);
        TextView title = view.findViewById(R.id.title);
        title.setText(field.getTitle());
        final EditText comment = view.findViewById(R.id.comment);
        view.setTag(fieldId);
        if (visit.getValues().containsKey(fieldId)) {
            comment.setText(visit.getStringValue(fieldId));
        }
        comment.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                lastEditText = (EditText) v;
                if (!hasFocus) {
                    updateVisitValue(fieldId, new VisitStringValue(comment.getText().toString()));
                }
            }
        });
        comment.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                for (TitleModel titleModel : viewTitleList) {
                    if (field.getId().equals(titleModel.getViewId()) && titleModel.getTitle().getCurrentTextColor()
                            == ContextCompat.getColor(getBaseContext(), R.color.colorException)) {
                        titleModel.getTitle().setText(titleModel.getTitle().getText().toString());
                        titleModel.getTitle().setTextColor(ContextCompat.getColor(getBaseContext(), R.color.grayColor));
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });
        enableOrDisableView(comment);
        addViewsEditing(comment);
        return view;
    }

    private void addViewToBlock(View view) {
        blockView.addView(view);
    }

    private void createDescriptionBlock(VisitTemplateBlock block) {
        TextView title = createTitle(block.getTitle(), block.isRequired());
        addViewToBlock(title);

        for (VisitTemplateBlockField field : block.getFields()) {
            LinearLayout description = createDescription(field);
            addViewToBlock(description);
        }
    }

    private void createPhotosBlock(VisitTemplateBlock block) {
        TitleModel titleModel;
        TextView title = createTitle(block.getTitle(), block.isRequired());
        addViewToBlock(title);

        HorizontalScrollView scroll = (HorizontalScrollView) inflateView(R.layout.visit_view_photos);
        addViewToBlock(scroll);
        LinearLayout container = scroll.findViewById(R.id.photos);
        for (VisitTemplateBlockField field : block.getFields()) {
            FrameLayout photo = createPhoto(field.getId());
            titleModel = new TitleModel(title, field.getId());
            viewTitleList.add(titleModel);
            container.addView(photo);
        }
    }

    private void createCheckersBlock(VisitTemplateBlock block) {
        TitleModel titleModel;
        TextView title = createTitle(block.getTitle(), block.isRequired());
        addViewToBlock(title);
        List<String> ids = new ArrayList<>();
        for (VisitTemplateBlockField field : block.getFields()) {
            CheckBox checkBox = createCheckbox(field.getId(), field.getTitle());
            ids.add(field.getId());
            addViewToBlock(checkBox);
        }
        titleModel = new TitleModel(title, ids);
        viewTitleList.add(titleModel);
    }

    private void createNumbersBlock(VisitTemplateBlock block) {
        TitleModel titleModel;
        TextView title = createTitle(block.getTitle(), block.isRequired());
        addViewToBlock(title);

        for (VisitTemplateBlockField field : block.getFields()) {
            LinearLayout number = createNumber(field);
            titleModel = new TitleModel(title, field.getId());
            viewTitleList.add(titleModel);
            addViewToBlock(number);
        }
    }

    private void createCommentsBlock(VisitTemplateBlock block) {
        TitleModel titleModel;
        TextView title = createTitle(block.getTitle(), block.isRequired());
        addViewToBlock(title);

        for (VisitTemplateBlockField field : block.getFields()) {
            LinearLayout comment = createComment(field);
            titleModel = new TitleModel(title, field.getId());
            viewTitleList.add(titleModel);
            addViewToBlock(comment);
        }
    }

    private void takePhotoFromCamera() {
        String dir = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Mrvk.pro";
        currentPhotoPath = ImageHelper.takePhotoFromCamera(this, CAMERA_RC, dir);
    }

    private void showPhoto(ImageView photo, String path, View delete, String id) {
        String tag = (String) photo.getTag();
        photo.setTag(null);
        Glide.with(this)
                .load(new File(path))
                .transform(new CenterCrop(), new RoundedCorners(10))
                .into(photo);
        photo.setTag(tag);
        for (TitleModel titleModel : viewTitleList) {
            if (id.equals(titleModel.getViewId()) && titleModel.getTitle().getCurrentTextColor()
                    == ContextCompat.getColor(getBaseContext(), R.color.colorException)) {
                titleModel.getTitle().setText(titleModel.getTitle().getText().toString());
                titleModel.getTitle().setTextColor(ContextCompat.getColor(getBaseContext(), R.color.grayColor));
            }
        }
        showOrHideView(delete, true);
    }

    private boolean checkLocation() {
        if (visit.isCheckGeo()) {
            if (LocationService.getLastLocationPoint() == null) {
                API.showToast(getString(R.string.could_not_determine_location));
                return false;
            }

            double dist;
            VisitPlace place = visit.getPlace();
            try {
                double lat1 = LocationService.getLastLocationPoint().getLatitude();
                double lon1 = LocationService.getLastLocationPoint().getLongitude();
                double lat2 = Double.valueOf(place.getLocation().split(",")[0]);
                double lon2 = Double.valueOf(place.getLocation().split(",")[1]);
                dist = API.calculateDistance(lat1, lon1, lat2, lon2);
            } catch (Exception e) {
                API.showToast(getString(R.string.could_not_determine_location), 1);
                return false;
            }
            if (dist < 0) {
                API.showToast(getString(R.string.could_not_determine_location), 1);
                return false;
            }
            if (dist > getPrefInteger(KEY_VISIT_POINT_RADIUS)) {
                API.showToast(getString(R.string.unable_to_start_audit), 1);
                return false;
            }
        }
        return true;
    }

    private void startVisit(LocationPoint location) {
        visit.setStatus(VisitStatus.IN_PROGRESS);
        visit.setStartTime(DateHelper.getCurrentIsoDate());
        visit.setStartPosition(location.getLatitude() + "," + location.getLongitude());
        DatabaseService.updateVisit(visit);

        JsonObject data = new JsonObject();
        data.addProperty("visitId", visit.getId());
        NetworkService.getInstance().postVisitStart(visit.getId())
                .enqueue(new Callback<VisitStartResDto>() {
                    @Override
                    public void onResponse(Call<VisitStartResDto> call, retrofit2.Response<VisitStartResDto> response) {

                    }

                    @Override
                    public void onFailure(Call<VisitStartResDto> call, Throwable t) {
                        t.printStackTrace();
                        if (t instanceof UnknownHostException) {
                            // все нормально, нет сети
                            return;
                        }
                        Crashlytics.logException(t);
                        API.showToast("CRASH: Ошибка при старте отчета", 5);
                    }
                });
    }

    @OnClick(R.id.btnStartVisit)
    public void onClickBtnStart() {
        if (visit.getStatus().equals(VisitStatus.TODO)) {
            if (!checkLocation()) {
                return;
            }
            LocationPoint location = LocationService.getLastLocationPoint();
            if (location == null) {
                API.showToast(getString(R.string.could_not_determine_location), 1);
            } else {
                allowEditing();
                startVisit(location);
                setupButtonStart();
            }
        } else if (visit.getStatus().equals(VisitStatus.IN_PROGRESS)) {
            lastEtClearFocus();
            if (!checkTemplateData()) {
                API.showToast(getString(R.string.need_fill_all_blocks), 1);
            } else {
                saveVisit();
            }
        }
    }

    private boolean checkTemplateData() {
        for (VisitTemplateBlock block : visit.getTemplate().getBlocks()) {
            if (!block.isRequired())
                continue;
            boolean isCheckersBlock = block.getType().equals(VisitTemplateBlock.TYPE_CHECKERS);
            boolean someChecked = false;
            for (VisitTemplateBlockField field : block.getFields()) {
                switch (block.getType()) {
                    case VisitTemplateBlock.TYPE_COMMENTS:
                        String comment = visit.getStringValue(field.getId());
                        if (comment == null || comment.trim().isEmpty()) {
                            for (TitleModel titleModel : viewTitleList) {
                                if (field.getId().equals(titleModel.getViewId())) {
                                    focusOnView(titleModel.getTitle());
                                    titleModel.getTitle().setText(titleModel.getTitle().getText().toString());
                                    titleModel.getTitle().setTextColor(ContextCompat.getColor(getBaseContext(), R.color.colorException));
                                }
                            }
                            return false;
                        }
                        break;
                    case VisitTemplateBlock.TYPE_NUMBERS:
                        Double number = visit.getDoubleValue(field.getId());
                        if (number == null) {
                            for (TitleModel titleModel : viewTitleList) {
                                if (field.getId().equals(titleModel.getViewId())) {
                                    focusOnView(titleModel.getTitle());
                                    titleModel.getTitle().setText(titleModel.getTitle().getText().toString());
                                    titleModel.getTitle().setTextColor(ContextCompat.getColor(getBaseContext(), R.color.colorException));
                                }
                            }
                            return false;
                        }
                        break;
                    case VisitTemplateBlock.TYPE_CHECKERS:
                        for (TitleModel titleModel : viewTitleList) {
                            if (titleModel.getViewIds() != null && !titleModel.getViewIds().isEmpty()) {
                                for (String idBlock : titleModel.getViewIds()) {

                                    if (field.getId().equals(idBlock)) {
                                        Boolean check = visit.getBooleanValue(idBlock);
                                        if (check != null && check) {
                                            titleModel.getTitle().setText(titleModel.getTitle().getText().toString());
                                            titleModel.getTitle().setTextColor(ContextCompat.getColor(getBaseContext(), R.color.grayColor));
                                            someChecked = true;
                                            break;
                                        } else {
                                            if (!someChecked) {
                                                focusOnView(titleModel.getTitle());
                                                titleModel.getTitle().setText(titleModel.getTitle().getText().toString());
                                                titleModel.getTitle().setTextColor(ContextCompat.getColor(getBaseContext(), R.color.colorException));
                                                someChecked = false;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        break;
                    case VisitTemplateBlock.TYPE_PHOTOS:
                        VisitPhotoValue photoValue = visit.getPhotoValue(field.getId());
                        if (photoValue == null || photoValue.getPathName() == null) {
                            for (TitleModel titleModel : viewTitleList) {
                                if (field.getId().equals(titleModel.getViewId())) {
                                    focusOnView(titleModel.getTitle());
                                    titleModel.getTitle().setText(titleModel.getTitle().getText().toString());
                                    titleModel.getTitle().setTextColor(ContextCompat.getColor(getBaseContext(), R.color.colorException));
                                }
                            }
                            return false;
                        }
                        break;
                }
            }
            if (isCheckersBlock && !someChecked) return false;
        }
        return true;
    }

    private void focusOnView(final View view) {
        nsContainer.post(new Runnable() {
            @Override
            public void run() {
                nsContainer.scrollTo(0, view.getTop());
            }
        });
    }

    private void saveVisit() {
        visit.setStatus(VisitStatus.SAVED);
        visit.setFinishTime(DateHelper.getCurrentIsoDate());
        LocationPoint location = LocationService.getLastLocationPoint();
        if (location != null) {
            visit.setFinishPosition(location.getLatitude() + "," + location.getLongitude());
        } else {
            visit.setFinishPosition(null);
        }
        DatabaseService.updateVisit(visit);
        EventBus.getDefault().post(new SavedVisitEvent(visit));
        finish();
    }

    private void lastEtClearFocus() {
        if (lastEditText != null) {
            lastEditText.clearFocus();
        }
    }

    public static void open(Visit visit, Context context) {
        Intent intent = new Intent(context, VisitActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("visit", new Gson().toJson(visit));
        context.startActivity(intent);
    }
}