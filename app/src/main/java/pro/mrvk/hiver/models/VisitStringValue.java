package pro.mrvk.hiver.models;

public class VisitStringValue extends VisitValue {

    public VisitStringValue(String value) {
        type = STRING_TYPE;
        stringValue = value;
    }

    public String getValue() {
        return stringValue;
    }
}
