package pro.mrvk.hiver.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VisitPlace {

    @Expose
    @SerializedName("id")
    private String id;

    @Expose
    @SerializedName("location")
    private String location;

    @Expose
    @SerializedName("name")
    private String name;

    @Expose
    @SerializedName("city")
    private String city;

    @Expose
    @SerializedName("address")
    private String address;

    @Expose
    @SerializedName("phone")
    private String phone;

    @Expose
    @SerializedName("orgName")
    private String orgName;

    @Expose
    @SerializedName("contactName")
    private String contactName;

    public String getId() {
        return id;
    }

    public String getLocation() {
        return location;
    }

    public String getName() {
        return name;
    }

    public String getCity() {
        return city;
    }

    public String getAddress() {
        return address;
    }

    public String getPhone() {
        return phone;
    }

    public String getOrgName() {
        return orgName;
    }

    public String getContactName() {
        return contactName;
    }
}
