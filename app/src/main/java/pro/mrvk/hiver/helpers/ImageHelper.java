package pro.mrvk.hiver.helpers;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;

import com.crashlytics.android.Crashlytics;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import static pro.mrvk.hiver.core.PrefencesApi.KEY_MAX_PHOTO_FILESIZE;
import static pro.mrvk.hiver.core.PrefencesApi.getPrefInteger;

public class ImageHelper {

    public static String takePhotoFromCamera(Activity activity, int requestCode, String dir) {
        File file = getCameraPhotoFile(dir);
        if (file == null) {
            return null;
        }
        Intent intent = getCameraPhotoIntent(activity, file);
        if (intent.resolveActivity(activity.getPackageManager()) != null) {
            activity.startActivityForResult(intent, requestCode);
        }
        return file.getAbsolutePath();
    }

    private static File getCameraPhotoFile(String dir) {
        File dirFile = new File(dir);
        if (!dirFile.exists()) {
            dirFile.mkdir();
        }
        try {
            String prefix = System.currentTimeMillis() + "";
            return File.createTempFile(prefix, ".jpg", dirFile);
        } catch (IOException e) {
            return null;
        }
    }

    private static Intent getCameraPhotoIntent(Context context, File file) {
        Uri uri = getUriFromFile(context, file);
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
        return intent;
    }

    private static Uri getUriFromFile(Context context, File file) {
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT) {
            return Uri.fromFile(file);
        } else {
            return FileProvider.getUriForFile(context,"pro.mrvk.hiver.provider", file);
        }
    }

    public static void compress(Context context, File file) {
        try {
            int quality = 90;
            Integer maxPhotoFilesize = getPrefInteger(KEY_MAX_PHOTO_FILESIZE);
            // FIXME: такого не должно случиться, стоит предусмотреть этот кейс
            if (maxPhotoFilesize == null) maxPhotoFilesize = 1_000_000;
            while (quality > 50 && file.length() > maxPhotoFilesize) {
                Uri uri = getUriFromFile(context, file);
                Bitmap bm = MediaStore.Images.Media.getBitmap(context.getContentResolver(), uri);
                bm = modifyOrientation(bm, file.getPath());

                FileOutputStream fos = new FileOutputStream(file);
                bm.compress(Bitmap.CompressFormat.JPEG, quality, fos);
                fos.flush();
                fos.close();
                quality -= 15;
            }
        } catch (Exception e) {
            e.printStackTrace();
            Crashlytics.logException(e);
        }
    }

    private static Bitmap modifyOrientation(Bitmap bitmap, String path) {
        try {
            ExifInterface ei = new ExifInterface(path);
            int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_NORMAL);
            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_90:
                    return rotate(bitmap, 90);
                case ExifInterface.ORIENTATION_ROTATE_180:
                    return rotate(bitmap, 180);
                case ExifInterface.ORIENTATION_ROTATE_270:
                    return rotate(bitmap, 270);
                case ExifInterface.ORIENTATION_FLIP_HORIZONTAL:
                    return flip(bitmap, true, false);
                case ExifInterface.ORIENTATION_FLIP_VERTICAL:
                    return flip(bitmap, false, true);
                default:
                    return bitmap;
            }
        } catch (Exception e) {
            return bitmap;
        }
    }

    private static Bitmap rotate(Bitmap bitmap, float degrees) {
        Matrix matrix = new Matrix();
        matrix.postRotate(degrees);
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
    }

    private static Bitmap flip(Bitmap bitmap, boolean horizontal, boolean vertical) {
        Matrix matrix = new Matrix();
        matrix.preScale(horizontal ? -1 : 1, vertical ? -1 : 1);
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
    }
}
