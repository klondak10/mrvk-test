package pro.mrvk.hiver.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class VisitTemplate {

    @Expose
    @SerializedName("id")
    private String id;

    @Expose
    @SerializedName("version")
    private String version;

    @Expose
    @SerializedName("title")
    private String title;

    @Expose
    @SerializedName("blocks")
    private List<VisitTemplateBlock> blocks;

    public String getId() {
        return id;
    }

    public String getVersion() {
        return version;
    }

    public String getTitle() {
        return title;
    }

    public List<VisitTemplateBlock> getBlocks() {
        return blocks;
    }
}
