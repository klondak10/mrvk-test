package pro.mrvk.hiver.ui.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;

import pro.mrvk.hiver.BuildConfig;
import pro.mrvk.hiver.R;
import pro.mrvk.hiver.models.UploadVisitsProgress;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.net.UnknownHostException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pro.mrvk.hiver.activities.LoginActivity;
import pro.mrvk.hiver.activities.VisitActivity;
import pro.mrvk.hiver.core.API;
import pro.mrvk.hiver.eventbus.RefreshVisitsEvent;
import pro.mrvk.hiver.helpers.PhotoManager;
import pro.mrvk.hiver.models.Visit;
import pro.mrvk.hiver.models.VisitStatus;
import pro.mrvk.hiver.network.dto.VisitRouteResDto;
import pro.mrvk.hiver.services.DatabaseService;
import pro.mrvk.hiver.services.NetworkService;
import pro.mrvk.hiver.ui.adapters.VisitAdapter;
import retrofit2.Call;
import retrofit2.Callback;

import static pro.mrvk.hiver.core.PrefencesApi.KEY_AUTH_TOKEN;
import static pro.mrvk.hiver.core.PrefencesApi.clearPref;

public class PlacesFragment extends Fragment implements VisitAdapter.Callback {

    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.labelFinished)
    TextView labelFinished;
    @BindView(R.id.labelWaiting)
    TextView labelWaiting;
    @BindView(R.id.labelWork)
    TextView labelWork;
    @BindView(R.id.orangeSyncButton)
    public RelativeLayout orangeSyncButton;
    @BindView(R.id.statusLinearLayout)
    LinearLayout statusLinearLayout;

    private PhotoManager photoManager = PhotoManager.get();

    private VisitAdapter mAdapter;
    private boolean isRouteLoadedFromServer;

    ArrayList<Visit> visitRoute = new ArrayList<>();

    private Handler handler;
    private final int PERIOD_LOAD_VISITS = 30_000;
    int count = 0;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        handler = new Handler();
        mAdapter = new VisitAdapter(this);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus eventBus = EventBus.getDefault();
        if (!eventBus.isRegistered(this)) eventBus.register(this);
        loadVisitRoute();
    }

    private Runnable doPeriodTask = new Runnable() {
        @Override
        public void run() {
            count += PERIOD_LOAD_VISITS;
            loadVisitRoute();
        }
    };

    @Override
    public void onStop() {
        handler.removeCallbacks(doPeriodTask);
        super.onStop();
    }

    @Override
    public void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = null;
        try {
            rootView = inflater.inflate(R.layout.fragment_places, container, false);
            ButterKnife.bind(this, rootView);
            int blue = R.color.blueColor;
            int green = R.color.greenColor;
            int org = R.color.orgColor;
            int orange = R.color.orangeColor;
            swipeRefreshLayout.setColorSchemeResources(blue, green, org, orange);
            swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    // когда свайпаем список вниз, должен обновиться маршрут
                    loadVisitRoute();
                    swipeRefreshLayout.setRefreshing(false);
                }
            });

            // в дебаг-режиме выводится номер версии
            if (BuildConfig.DEBUG) {
                TextView debugInfoLabel = new TextView(this.getContext());
                debugInfoLabel.setText("v" + BuildConfig.VERSION_NAME);
                debugInfoLabel.setBackgroundColor(0xff66ff66);
                debugInfoLabel.setPadding(10, 10, 10, 10);

                LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                lparams.gravity = Gravity.RIGHT;
                debugInfoLabel.setLayoutParams(lparams);

                statusLinearLayout.addView(debugInfoLabel);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Crashlytics.logException(e);
        }
        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // setup recycler view
        mAdapter = new VisitAdapter(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onClickItem(Visit visit) {
        if (isRouteLoadedFromServer) {
            double time = (new java.util.Date().getTime() - Date.valueOf(visit.getReportDate()).getTime()) / 86400000d;
            if (time > 1) {
                API.showToast("Время для выполнения уже вышло", 3);
            } else if (time < 0) {
                API.showToast("Время для выполнения еще не наступило", 3);
            } else if (visit.getStatus().equals(VisitStatus.TODO) || visit.getStatus().equals(VisitStatus.IN_PROGRESS)) {
                VisitActivity.open(visit, getContext());
            }
        } else {
            if ((visit.getStatus().equals(VisitStatus.TODO) || visit.getStatus().equals(VisitStatus.IN_PROGRESS)) && visit.isAvailableToReport()) {
                VisitActivity.open(visit, getContext());
            }
        }
    }

    public void loadVisitRoute() {
        // стопаем таймер по обновлению маршрута
        handler.removeCallbacks(doPeriodTask);
        new Thread(new Runnable() {
            @Override
            public void run() {
                NetworkService.getInstance().getVisitRoute(getDate())
                        .enqueue(new Callback<VisitRouteResDto>() {
                            @Override
                            public void onResponse(Call<VisitRouteResDto> call, retrofit2.Response<VisitRouteResDto> response) {
                                if (response.code() == 403) {
                                    clearPref(KEY_AUTH_TOKEN);
                                    API.showToast("Ошибка авторизации");
                                    startActivity(new Intent(getActivity(), LoginActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                                    return;
                                }
                                VisitRouteResDto routeData = response.body();
                                if (routeData == null) {
                                    API.showToast("CRASH: Ошибка получения маршрута: " + response.code() + " " + response.message(), 5);
                                    return;
                                }
                                String error = routeData.getError();
                                if (error != null) {
                                    API.showToast("CRASH: Ошибка получения маршрута: " + routeData.getError(), 5);
                                    return;
                                }
                                parseVisitRoute(routeData.getVisits());
                                // заново запускаем таймер обновления маршрута
                                handler.postDelayed(doPeriodTask, PERIOD_LOAD_VISITS);
                            }

                            @Override
                            public void onFailure(Call<VisitRouteResDto> call, Throwable t) {
                                t.printStackTrace();
                                if (t instanceof UnknownHostException) {
                                    // все нормально, нет сети
                                    loadLocalPlaces();
                                    return;
                                }
                                Crashlytics.logException(t);
                                API.showToast("CRASH: Ошибка загрузки маршрута", 5);
                            }
                        });
            }
        }).start();
    }

    void parseVisitRoute(final List<Visit> visits) {
        String date = getDate();
        for (Visit visit : visits) {
            visit.setReportDate(date);
            // FIXME
            if (visit.getStatus() == VisitStatus.IN_PROGRESS) {
                visit.setStatus(VisitStatus.TODO);
            }
        }

        visitRoute.clear();
        visitRoute.addAll(visits);

        saveVisits();
        fillVisitsFromDatabase();

        mAdapter.setList(visitRoute);

        isRouteLoadedFromServer = true;
    }

    private void fillVisitsFromDatabase() {
        List<Visit> dbList = DatabaseService.getVisitsByDate(getDate());
        List<Visit> newList = new ArrayList<>();

        for (int i = 0; i < dbList.size(); i++) {
            Visit dbVisit = dbList.get(i);
            boolean exist = false;
            for (int j = 0; j < visitRoute.size(); j++) {
                Visit serverVisit = visitRoute.get(j);
                if (dbVisit.getId().equals(serverVisit.getId())) {
                    exist = true;
                }
            }
            if (!exist) {
                newList.add(dbVisit);
            }
        }
        visitRoute.addAll(newList);
    }

    void loadLocalPlaces() {
        try {
            visitRoute = DatabaseService.getVisitsByDate(getDate());
            mAdapter.setList(visitRoute);

            int countFinish = 0;
            int countWork = 0;
            int countWaiting = DatabaseService.countSavedOrReportedVisits();
            for (int i = 0; i < visitRoute.size(); i++)
                if (visitRoute.get(i).getStatus().equals(VisitStatus.DONE))
                    countFinish++;
                else if (visitRoute.get(i).getStatus().equals(VisitStatus.TODO) || visitRoute.get(i).getStatus().equals(VisitStatus.IN_PROGRESS))
                    countWork++;
            labelFinished.setText("Завершено: " + countFinish);
            labelWaiting.setText("Ожидает отправки: " + countWaiting);
            labelWork.setText("Не выполнено: " + countWork);
            isRouteLoadedFromServer = false;
        } catch (Exception e) {
            e.printStackTrace();
            Crashlytics.logException(e);
            API.showToast("CRASH: Не загрузился локальный маршрут", 5);
        }
    }

    void saveVisits() {
        try {
            String date = getDate();
            int countFinish = 0;
            int countWork = 0;
            int countWaiting = DatabaseService.countSavedOrReportedVisits();
            ArrayList<Visit> hasVisits = DatabaseService.getVisitsByDate(date);
            for (int i = 0; i < hasVisits.size(); i++) {
                boolean hasOnServer = false;
                for (int k = 0; k < visitRoute.size(); k++) {
                    if (hasVisits.get(i).getId().equals(visitRoute.get(k).getId())) {
                        hasOnServer = true;
                        visitRoute.get(k).setStatus(hasVisits.get(i).getStatus());
                        visitRoute.get(k).setUpdatedAt(hasVisits.get(i).getUpdatedAt());
                    }
                }
                if (!hasOnServer && hasVisits.get(i).getStatus().equals(VisitStatus.TODO))
                    DatabaseService.removeVisit(hasVisits.get(i).getId());
                if (hasVisits.size() == visitRoute.size()) {
                    if (hasVisits.get(i).getStatus().equals(VisitStatus.DONE)) {
                        countFinish++;
                    } else if (hasVisits.get(i).getStatus().equals(VisitStatus.TODO) || hasVisits.get(i).getStatus().equals(VisitStatus.IN_PROGRESS)) {
                        countWork++;
                    }
                }
            }

            if (visitRoute.size() != hasVisits.size()) {
                for (int i = 0; i < visitRoute.size(); i++) {
                    if (visitRoute.get(i).getStatus().equals(VisitStatus.DONE)) {
                        countFinish++;
                    } else if (visitRoute.get(i).getStatus().equals(VisitStatus.TODO)) {
                        countWork++;
                    }
                }
            }

            if (hasVisits.size() > visitRoute.size()) {
                for (int j = 0; j < hasVisits.size(); j++) {
                    for (int i = 0; i < visitRoute.size(); i++) {
                        if (!hasVisits.get(j).getId().equals(visitRoute.get(i).getId())) {
                            if (hasVisits.get(i).getStatus().equals(VisitStatus.TODO)) {
                                DatabaseService.removeVisit(hasVisits.get(i).getId());
                            }
                        }
                    }
                }
            }

            labelFinished.setText("Завершено: " + countFinish);
            labelWaiting.setText("Ожидает отправки: " + countWaiting);
            labelWork.setText("Не выполнено: " + countWork);

            orangeSyncButton.setVisibility(countWaiting > 0 && !photoManager.isSending() ? View.VISIBLE : View.GONE);
            for (int i = 0; i < visitRoute.size(); i++) {
                boolean hasOnDb = false;
                for (int k = 0; k < hasVisits.size(); k++) {
                    if (visitRoute.get(i).getId().equals(hasVisits.get(k).getId()))
                        hasOnDb = true;
                }
                if (!hasOnDb) {
                    DatabaseService.addVisit(visitRoute.get(i));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            Crashlytics.logException(e);
        }
    }

    private String getDate() {
        return getArguments().getString("date");
    }

    @OnClick(R.id.orangeSyncButton)
    public void onSyncClick() {
        orangeSyncButton.setVisibility(View.GONE);
        photoManager.sendSavedOrReportedVisits();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadingVisitEvent(UploadVisitsProgress event) {
        mAdapter.updateProgress(event);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onRefreshVisitsEvent(RefreshVisitsEvent event) {
        loadVisitRoute();
    }
}