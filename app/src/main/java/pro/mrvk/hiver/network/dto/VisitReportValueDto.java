package pro.mrvk.hiver.network.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class VisitReportValueDto {
    @Expose
    @SerializedName("blockId")
    private String blockId;

    @Expose
    @SerializedName("fields")
    private List<VisitReportValueFieldDto> fields;

    public VisitReportValueDto(String blockId, List<VisitReportValueFieldDto> fields) {
        this.blockId = blockId;
        this.fields = fields;
    }
}
