package pro.mrvk.hiver.network.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VisitUploadReqDto {
    @Expose
    @SerializedName("visitId")
    private String visitId;

    @Expose
    @SerializedName("blockId")
    private String blockId;

    @Expose
    @SerializedName("fieldId")
    private String fieldId;

    public VisitUploadReqDto(String visitId, String blockId, String fieldId) {
        this.visitId = visitId;
        this.blockId = blockId;
        this.fieldId = fieldId;
    }
}
