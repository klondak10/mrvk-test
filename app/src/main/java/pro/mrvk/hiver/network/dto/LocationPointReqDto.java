package pro.mrvk.hiver.network.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.TimeZone;

import pro.mrvk.hiver.models.LocationPoint;

public class LocationPointReqDto {
    @Expose
    @SerializedName("date")
    private String date;

    @Expose
    @SerializedName("service")
    private String serviceName;

    @Expose
    @SerializedName("longitude")
    private double longitude;

    @Expose
    @SerializedName("latitude")
    private double latitude;

    @Expose
    @SerializedName("deltaTime")
    private long deltaTime;

    @Expose
    @SerializedName("distance")
    private double distance;

    private LocationPointReqDto() {
    }

    static public LocationPointReqDto of(LocationPoint locationPoint) {
        LocationPointReqDto dto = new LocationPointReqDto();
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.getDefault());
        df.setTimeZone(TimeZone.getTimeZone("GMT"));

        dto.date = df.format(locationPoint.getDate().getTime());
        dto.serviceName = locationPoint.getServiceName();
        dto.longitude = locationPoint.getLongitude();
        dto.latitude = locationPoint.getLatitude();
        dto.deltaTime = locationPoint.getDeltaTime();
        dto.distance = locationPoint.getDistance();
        return dto;
    }
}
