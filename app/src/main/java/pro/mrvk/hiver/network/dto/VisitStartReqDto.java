package pro.mrvk.hiver.network.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VisitStartReqDto {
    @Expose
    @SerializedName("visitId")
    private String visitId;

    public VisitStartReqDto(String visitId) {
        this.visitId = visitId;
    }
}
