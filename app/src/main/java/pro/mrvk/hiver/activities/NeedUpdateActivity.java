package pro.mrvk.hiver.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.fabric.sdk.android.Fabric;
import pro.mrvk.hiver.BuildConfig;
import pro.mrvk.hiver.R;

public class NeedUpdateActivity extends AppCompatActivity {

    static private final String TAG = "MRVK_NeedUpdateActv";

    @BindView(R.id.versionLabel)
    TextView versionLabel;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate");
        Fabric.with(this, new Crashlytics());

        setContentView(R.layout.activity_need_update);
        ButterKnife.bind(this);
        versionLabel.setText("v" + BuildConfig.VERSION_NAME);
    }

    @OnClick(R.id.btnUpdateAppVersion)
    public void ButtonUpdateAppVersionClick() {
        final String appPackageName = getPackageName();
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
        }
    }
}
