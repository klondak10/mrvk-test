package pro.mrvk.hiver.models;

import java.util.ArrayList;
import java.util.List;

public class VisitUpload {

    private String visitId;
    private String blockId;
    private String fieldId;
    private String photoFile;
    private boolean isSended;

    public VisitUpload(String visitId, String blockId, String fieldId, String photoFile, boolean isSended) {
        this.visitId = visitId;
        this.blockId = blockId;
        this.fieldId = fieldId;
        this.photoFile = photoFile;
        this.isSended = isSended;
    }

    public String getVisitId() {
        return visitId;
    }

    public String getBlockId() {
        return blockId;
    }

    public String getFieldId() {
        return fieldId;
    }

    public String getPhotoFile() {
        return photoFile;
    }

    public boolean isSended() {
        return isSended;
    }

    public static List<VisitUpload> of(Visit visit) {
        List<VisitUpload> uploads = new ArrayList<>();
        for (VisitTemplateBlock block : visit.getTemplate().getBlocks()) {
            if (!block.getType().equals(VisitTemplateBlock.TYPE_PHOTOS)) {
                continue;
            }
            for (VisitTemplateBlockField field : block.getFields()) {
                String fieldId = field.getId();
                VisitPhotoValue value = visit.getPhotoValue(fieldId);
                if (value != null) {
                    VisitUpload upload = new VisitUpload(visit.getId(), block.getId(), fieldId, value.getPathName(), value.isSending());
                    uploads.add(upload);
                }
            }
        }
        return uploads;
    }
}
