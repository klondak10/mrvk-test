package pro.mrvk.hiver.core;

import android.widget.Toast;

import com.crashlytics.android.Crashlytics;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import pro.mrvk.hiver.app.App;
import pro.mrvk.hiver.network.dto.LogResDto;
import pro.mrvk.hiver.services.NetworkService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class API {
    private static final double EARTH_RADIUS = 6371000; // средний радиус планеты в метрах

    // close constructor
    private API() {
    }

    private static double degrees2radians(double degrees) {
        return degrees * Math.PI / 180;
    }

    /**
     * Расчет расстояния по gps координатам
     *
     * @return Возвращает расстояние между точками в метрах
     */
    public static double calculateDistance(double lat1, double lon1, double lat2, double lon2) {
        double dLat = degrees2radians(lat1 - lat2);
        double dLon = degrees2radians(lon1 - lon2);
        lat1 = degrees2radians(lat1);
        lat2 = degrees2radians(lat2);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                Math.sin(dLon / 2) * Math.sin(dLon / 2) * Math.cos(lat1) * Math.cos(lat2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        return EARTH_RADIUS * c;
    }

    public static void showToast(String mess) {
        Toast.makeText(App.getContext(), mess, Toast.LENGTH_LONG).show();
    }

    public static void showToast(String mess, int length) {
        Toast.makeText(App.getContext(), mess, length).show();
    }

    /**
     * Копирование файла
     */
    public static void copyFile(File src, File dst) throws IOException {
        InputStream in = new FileInputStream(src);
        try {
            OutputStream out = new FileOutputStream(dst);
            try {
                // Transfer bytes from in to out
                byte[] buf = new byte[1024];
                int len;
                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }
            } finally {
                out.close();
            }
        } finally {
            in.close();
        }
    }

    public static void copyFile(String src, String dest) throws IOException {
        copyFile(new File(src), new File(dest));
    }

    /**
     * Отправка лога на сервер
     * @param type
     * @param message
     * @param reason
     */
    public static void sendLogToServer(String type, String message, String reason) {
        NetworkService.getInstance().sendLog(type, message, reason)
                .enqueue(new Callback<LogResDto>() {
                    @Override
                    public void onResponse(Call<LogResDto> call, Response<LogResDto> response) {
                    }

                    @Override
                    public void onFailure(Call<LogResDto> call, Throwable t) {
                        t.printStackTrace();
                        Crashlytics.logException(t);
                    }
                });
    }
}
