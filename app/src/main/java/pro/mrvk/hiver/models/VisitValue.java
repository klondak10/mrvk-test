package pro.mrvk.hiver.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VisitValue {
    static final String STRING_TYPE = "VisitStringValue";

    static final String DOUBLE_TYPE = "VisitDoubleValue";

    static final String BOOLEAN_TYPE = "VisitBooleanValue";

    static final String PHOTO_TYPE = "VisitPhotoValue";

    @Expose
    @SerializedName("type")
    protected String type;

    @Expose
    @SerializedName("boolean_value")
    protected Boolean booleanValue;

    @Expose
    @SerializedName("string_value")
    protected String stringValue;

    @Expose
    @SerializedName("double_value")
    protected Double doubleValue;

    public VisitValue get() {
        switch (type) {
            case STRING_TYPE:
                return new VisitStringValue(stringValue);

            case BOOLEAN_TYPE:
                return new VisitBooleanValue(booleanValue);

            case DOUBLE_TYPE:
                return new VisitDoubleValue(doubleValue);

            case PHOTO_TYPE:
                return new VisitPhotoValue(stringValue, booleanValue);

            default:
                return null;
        }
    }
}
