package pro.mrvk.hiver.network.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

public abstract class BaseResDto {
    @Expose
    @SerializedName("time")
    private Date time;

    @Expose
    @SerializedName("error")
    private String error;

    @Expose
    @SerializedName("error_const")
    private String errorConst;

    public String getError() {
        return error;
    }

    public String getErrorConst() {
        return errorConst;
    }
}
