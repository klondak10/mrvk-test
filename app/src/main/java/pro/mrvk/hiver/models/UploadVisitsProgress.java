package pro.mrvk.hiver.models;

import java.util.HashMap;
import java.util.Set;

public class UploadVisitsProgress {

    private class Counter {
        private int count = 0;
        private int sendedCount = 0;
        private int notSendCount = 0;

        public int getCount() {
            return count;
        }

        public void setCount(int count) {
            this.count = count;
        }

        public int getSendedCount() {
            return sendedCount;
        }

        public void setSendedCount(int sendedCount) {
            this.sendedCount = sendedCount;
        }

        public int getNotSendCount() {
            return notSendCount;
        }

        public void setNotSendCount(int notSendCount) {
            this.notSendCount = notSendCount;
        }
    }

    private HashMap<String, Counter> counters = new HashMap<>();

    private Counter get(String visitId) {
        if (!counters.containsKey(visitId)) {
            counters.put(visitId, new Counter());
        }
        return counters.get(visitId);
    }

    public void remove(Integer visitId) {
        if (counters.containsKey(visitId)) {
            counters.remove(visitId);
        }
    }

    public String[] getVisitIds() {
        Set<String> keys = counters.keySet();
        return keys.toArray(new String[keys.size()]);
    }

    public int getCount(String visitId) {
        return get(visitId).getCount();
    }

    public int getSendedCount(String visitId) {
        return get(visitId).getSendedCount();
    }

    public int getProgress(String visitId) {
        return (int) (100.0 / getCount(visitId) * getSendedCount(visitId));
    }

    public int getNotSendCount(String visitId) {
        return get(visitId).getNotSendCount();
    }

    public void setCount(String visitId, int count) {
        get(visitId).setCount(count);
    }

    public void setSendedCount(String visitId, int count) {
        get(visitId).setSendedCount(count);
    }

    public void setNotSendCount(String visitId, int count) {
        get(visitId).setNotSendCount(count);
    }
}
