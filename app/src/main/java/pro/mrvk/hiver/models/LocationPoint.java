package pro.mrvk.hiver.models;

import android.location.Location;

import java.util.Date;

public class LocationPoint {

    private Location location;

    private Date date;

    private long deltaTime;

    private String serviceName;

    private double distance;

    public LocationPoint(Location location, Date date, long deltaTime, String serviceName,
                         double distance) {
        this.location = location;
        this.date = date;
        this.deltaTime = deltaTime;
        this.serviceName = serviceName;
        this.distance = distance;
    }

    public double getLatitude() {
        return location.getLatitude();
    }

    public double getLongitude() {
        return location.getLongitude();
    }

    public Date getDate() {
        return date;
    }

    public long getDeltaTime() {
        return deltaTime;
    }

    public String getServiceName() {
        return serviceName;
    }

    public double getDistance() {
        return distance;
    }
}