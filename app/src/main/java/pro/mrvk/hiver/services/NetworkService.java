package pro.mrvk.hiver.services;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.Dispatcher;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.logging.HttpLoggingInterceptor;
import pro.mrvk.hiver.BuildConfig;
import pro.mrvk.hiver.Configuration;
import pro.mrvk.hiver.models.LocationPointList;
import pro.mrvk.hiver.models.Visit;
import pro.mrvk.hiver.network.LogRouteApi;
import pro.mrvk.hiver.network.PublicRouteApi;
import pro.mrvk.hiver.network.LocationRouteApi;
import pro.mrvk.hiver.network.VisitRouteApi;
import pro.mrvk.hiver.network.dto.LocationPointListReqDto;
import pro.mrvk.hiver.network.dto.LocationPointListResDto;
import pro.mrvk.hiver.network.dto.LogReqDto;
import pro.mrvk.hiver.network.dto.LogResDto;
import pro.mrvk.hiver.network.dto.VisitFinishReqDto;
import pro.mrvk.hiver.network.dto.VisitFinishResDto;
import pro.mrvk.hiver.network.dto.VisitReportReqDto;
import pro.mrvk.hiver.network.dto.VisitReportResDto;
import pro.mrvk.hiver.network.dto.VisitRouteResDto;
import pro.mrvk.hiver.network.dto.VisitStartReqDto;
import pro.mrvk.hiver.network.dto.VisitStartResDto;
import pro.mrvk.hiver.network.dto.VisitUploadResDto;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

import static pro.mrvk.hiver.core.PrefencesApi.KEY_AUTH_TOKEN;
import static pro.mrvk.hiver.core.PrefencesApi.getPrefString;

public class NetworkService {
    private Retrofit retrofitInstance;

    private NetworkService() {
        Dispatcher dispatcher = new Dispatcher();
        dispatcher.setMaxRequests(3);

//        OkHttpClient client = new OkHttpClient.Builder()
//                .addInterceptor(new Interceptor() {
//                    @Override
//                    public okhttp3.Response intercept(Chain chain) throws IOException {
//                        Request original = chain.request();
//                        return chain.proceed(original.newBuilder()
//                                .header("Auth-Version", String.valueOf(BuildConfig.VERSION_CODE))
//                                .header("Accept", "application/json")
//                                .header("Accept-Charset", "utf-8, iso-8859-1;q=0.5")
//                                .method(original.method(), original.body())
//                                .build()
//                        );
//                    }
//                })
//                .dispatcher(dispatcher)
//                .retryOnConnectionFailure(true)
//                .connectTimeout(60, TimeUnit.SECONDS)
//                .readTimeout(60, TimeUnit.SECONDS)
//                .writeTimeout(10, TimeUnit.MINUTES)
//                .build();
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        retrofitInstance = new Retrofit.Builder()
                .baseUrl(Configuration.BACKEND_API_URL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(client)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    static private NetworkService instance;

    static public NetworkService getInstance() {
        if (instance == null) {
            synchronized (NetworkService.class) {
                if (instance == null) {
                    instance = new NetworkService();
                }
            }
        }
        return instance;
    }

    //
    // api getters
    //

    public PublicRouteApi getPublicApi() {
        return retrofitInstance.create(PublicRouteApi.class);
    }

    public VisitRouteApi getVisitApi() {
        return retrofitInstance.create(VisitRouteApi.class);
    }

    public LocationRouteApi getStatisticsApi() {
        return retrofitInstance.create(LocationRouteApi.class);
    }

    public LogRouteApi getLogApi() {
        return retrofitInstance.create(LogRouteApi.class);
    }

    //
    // make headers
    //

    public Map<String, String> getAuthHeaders() {
        HashMap<String, String> headers = new HashMap<>();
        headers.put("Auth-Token", getPrefString(KEY_AUTH_TOKEN));
        headers.put("Auth-Version", String.valueOf(BuildConfig.VERSION_CODE));
        return headers;
    }

    //
    // Visit API Route callers
    //

    // Fetch visit route with GET /api/visits/route
    public Call<VisitRouteResDto> getVisitRoute(String date) {
        return getVisitApi().getVisitRoute(getAuthHeaders(), date);
    }

    // Change status with POST /api/visits/start
    public Call<VisitStartResDto> postVisitStart(String visitId) {
        return getVisitApi().postVisitStart(getAuthHeaders(), new VisitStartReqDto(visitId));
    }

    // Report visit with POST /api/visits/report
    public Call<VisitReportResDto> postVisitReport(Visit visit) {
        return getVisitApi().postVisitReport(getAuthHeaders(), VisitReportReqDto.of(visit));
    }

    // Upload photo with POST /api/visits/upload
    public Call<VisitUploadResDto> postVisitUpload(String visitId, String blockId, String fieldId, MultipartBody.Part file) {
        return getVisitApi().postVisitUpload(
                getAuthHeaders(),
                RequestBody.create(MediaType.parse("text/plain"), visitId),
                RequestBody.create(MediaType.parse("text/plain"), blockId),
                RequestBody.create(MediaType.parse("text/plain"), fieldId),
                file);
    }

    // Change status with POST /api/visits/finish
    public Call<VisitFinishResDto> postVisitFinish(String visitId) {
        return getVisitApi().postVisitFinish(getAuthHeaders(), new VisitFinishReqDto(visitId));
    }

    //
    // Location API Route callers
    //

    // Update location point
    public Call<LocationPointListResDto> sendLocationPoint(LocationPointList locationPoints) {
        return getStatisticsApi()
                .sendLocationPoint(getAuthHeaders(), LocationPointListReqDto.of(locationPoints));
    }

    //
    // Log API Route callers
    //

    // Send log message
    public Call<LogResDto> sendLog(String type, String message, String reason) {
        return getLogApi()
                .sendLog(getAuthHeaders(), new LogReqDto(type, message, reason));
    }
}
