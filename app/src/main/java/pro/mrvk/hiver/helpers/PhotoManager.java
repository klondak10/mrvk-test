package pro.mrvk.hiver.helpers;

import android.annotation.SuppressLint;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;

import pro.mrvk.hiver.app.App;
import pro.mrvk.hiver.services.PhotoService;

public class PhotoManager {

    @SuppressLint("StaticFieldLeak")
    private static volatile PhotoManager singleton;
    private PhotoService photoService;
    private boolean serviceBound;

    private PhotoManager() {
    }

    public static PhotoManager get() {
        if (singleton == null) {
            synchronized (PhotoManager.class) {
                if (singleton == null) {
                    singleton = new PhotoManager();
                }
            }
        }
        return singleton;
    }

    public void bind() {
        Context context = App.getContext();
        Intent intent = new Intent(context, PhotoService.class);
        context.bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);
    }

    public void sendSavedOrReportedVisits() {
        photoService.sendSavedOrReportedVisits();
    }

    public boolean isSending() {
        return photoService.isSending();
    }

    private final ServiceConnection serviceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName componentName, IBinder binder) {
            photoService = ((PhotoService.PhotoBinder) binder).getService();
            serviceBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            serviceBound = false;
        }
    };
}
