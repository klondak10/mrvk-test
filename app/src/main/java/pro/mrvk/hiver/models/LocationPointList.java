package pro.mrvk.hiver.models;

import java.util.ArrayList;
import java.util.List;

public class LocationPointList {

    private List<LocationPoint> locationPoints;

    public LocationPointList(List<LocationPoint> locationPoints) {
        this.locationPoints = locationPoints;
    }

    public List<LocationPoint> getLocationPoints() {
        if (locationPoints == null) {
            locationPoints = new ArrayList<>();
        }
        return locationPoints;
    }

    public void add(LocationPoint locationPoint) {
        this.getLocationPoints().add(locationPoint);
    }
}