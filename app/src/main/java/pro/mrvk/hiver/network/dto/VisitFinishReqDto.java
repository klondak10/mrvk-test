package pro.mrvk.hiver.network.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VisitFinishReqDto {
    @Expose
    @SerializedName("visitId")
    private String visitId;

    public VisitFinishReqDto(String visitId) {
        this.visitId = visitId;
    }
}
