package pro.mrvk.hiver.services;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import pro.mrvk.hiver.BuildConfig;
import pro.mrvk.hiver.app.App;
import pro.mrvk.hiver.models.Visit;
import pro.mrvk.hiver.models.VisitPlace;
import pro.mrvk.hiver.models.VisitStatus;
import pro.mrvk.hiver.models.VisitTemplate;
import pro.mrvk.hiver.models.VisitValue;

public class DatabaseService extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "mrvk" + BuildConfig.VERSION_CODE + ".db";
    private static final int DATABASE_VERSION = BuildConfig.VERSION_CODE;

    private static final String VISITS_TABLE_NAME = "visits";

    private static final String KEY_VISIT_ID = "visit_id";
    private static final String KEY_PLACE = "place";
    private static final String KEY_TEMPLATE = "template";
    private static final String KEY_REPORT_DATE = "report_date";
    private static final String KEY_STATUS = "status";
    private static final String KEY_STATUS_NUMBER = "status_number";
    private static final String KEY_AVAILABLE_TO_REPORT = "available_to_report";
    private static final String KEY_IS_CHECK_GEO = "is_check_geo";
    private static final String KEY_START_TIME = "start_time";
    private static final String KEY_FINISH_TIME = "finish_time";
    private static final String KEY_START_POSITION = "start_position";
    private static final String KEY_FINISH_POSITION = "finish_position";
    private static final String KEY_VALUES = "visit_values";
    private static final String KEY_IS_QUERING = "is_quering";
    private static final String KEY_UPDATED_AT = "updated_at";

    private DatabaseService() {
        super(App.getContext(), DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS " + VISITS_TABLE_NAME + " (" +
                KEY_VISIT_ID + " STRING, " +
                KEY_PLACE + " TEXT, " +
                KEY_TEMPLATE + " TEXT, " +
                KEY_REPORT_DATE + " TEXT, " +
                KEY_STATUS + " TEXT, " +
                KEY_STATUS_NUMBER + " INTEGER, " +
                KEY_AVAILABLE_TO_REPORT + " INTEGER, " +
                KEY_IS_CHECK_GEO + " INTEGER, " +
                KEY_START_TIME + " TEXT, " +
                KEY_FINISH_TIME + " TEXT, " +
                KEY_START_POSITION + " TEXT, " +
                KEY_FINISH_POSITION + " TEXT, " +
                KEY_VALUES + " TEXT, " +
                KEY_IS_QUERING + " INTEGER, " +
                KEY_UPDATED_AT + " INTEGER)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onCreate(db);
    }

    static public int countVisitsRows() {
        Cursor cursor = new DatabaseService().getWritableDatabase().query(VISITS_TABLE_NAME, null, null, null, null, null, null);
        if (cursor.getCount() > 0) {
            cursor.close();
            return cursor.getCount();
        } else {
            cursor.close();
            return 0;
        }
    }

    static public void addVisit(Visit visit) {
        ContentValues cv = new ContentValues();
        cv.put(KEY_VISIT_ID, visit.getId());
        cv.put(KEY_PLACE, new Gson().toJson(visit.getPlace()));
        cv.put(KEY_TEMPLATE, new Gson().toJson(visit.getTemplate()));
        cv.put(KEY_REPORT_DATE, visit.getReportDate());
        cv.put(KEY_STATUS, visit.getStatus().getName());
        cv.put(KEY_STATUS_NUMBER, visit.getStatus().getCode());
        cv.put(KEY_AVAILABLE_TO_REPORT, visit.isAvailableToReport() ? 1 : 0);
        cv.put(KEY_IS_CHECK_GEO, visit.isCheckGeo() ? 1 : 0);
        cv.put(KEY_UPDATED_AT, visit.getUpdatedAt());
        // insert
        SQLiteDatabase db = new DatabaseService().getWritableDatabase();
        db.insert(VISITS_TABLE_NAME, null, cv);
        db.close();
    }

    static public void updateVisit(Visit visit) {
        ContentValues cv = new ContentValues();
        cv.put(KEY_PLACE, new Gson().toJson(visit.getPlace()));
        cv.put(KEY_TEMPLATE, new Gson().toJson(visit.getTemplate()));
        cv.put(KEY_REPORT_DATE, visit.getReportDate());
        cv.put(KEY_STATUS, visit.getStatus().getName());
        cv.put(KEY_STATUS_NUMBER, visit.getStatus().getCode());
        cv.put(KEY_AVAILABLE_TO_REPORT, visit.isAvailableToReport() ? 1 : 0);
        cv.put(KEY_IS_CHECK_GEO, visit.isCheckGeo() ? 1 : 0);
        cv.put(KEY_START_TIME, visit.getStartTime());
        cv.put(KEY_FINISH_TIME, visit.getFinishTime());
        cv.put(KEY_START_POSITION, visit.getStartPosition());
        cv.put(KEY_FINISH_POSITION, visit.getFinishPosition());
        cv.put(KEY_VALUES, new Gson().toJson(visit.getValues()));
        cv.put(KEY_IS_QUERING, visit.isQuering() ? 1 : 0);
        if (visit.getStatus().equals(VisitStatus.IN_PROGRESS) || visit.getStatus().equals(VisitStatus.SAVED)) {
            cv.put(KEY_UPDATED_AT, Calendar.getInstance().getTimeInMillis());
        }
        // update
        SQLiteDatabase db = new DatabaseService().getWritableDatabase();
        db.update(VISITS_TABLE_NAME, cv, KEY_VISIT_ID + " = '" + visit.getId() + "'", null);
        db.close();
    }

    static public void updateVisitStatus(Visit visit) {
        ContentValues cv = new ContentValues();
        cv.put(KEY_STATUS, visit.getStatus().getName());
        cv.put(KEY_STATUS_NUMBER, visit.getStatus().getCode());
        if (visit.getStatus().equals(VisitStatus.IN_PROGRESS) || visit.getStatus().equals(VisitStatus.SAVED)) {
            cv.put(KEY_UPDATED_AT, Calendar.getInstance().getTimeInMillis());
        }
        SQLiteDatabase db = new DatabaseService().getWritableDatabase();
        db.update(VISITS_TABLE_NAME, cv, KEY_VISIT_ID + " = '" + visit.getId() + "'", null);
        db.close();
    }

    static public void updateVisitValues(Visit visit) {
        ContentValues cv = new ContentValues();
        cv.put(KEY_VALUES, new Gson().toJson(visit.getValues()));
        if (visit.getStatus().equals(VisitStatus.IN_PROGRESS) || visit.getStatus().equals(VisitStatus.SAVED)) {
            cv.put(KEY_UPDATED_AT, Calendar.getInstance().getTimeInMillis());
        }
        SQLiteDatabase db = new DatabaseService().getWritableDatabase();
        db.update(VISITS_TABLE_NAME, cv, KEY_VISIT_ID + " = '" + visit.getId() + "'", null);
        db.close();
    }

    static public void setVisitQuering(String visitId) {
        ContentValues cv = new ContentValues();
        cv.put(KEY_IS_QUERING, 1);
        SQLiteDatabase db = new DatabaseService().getWritableDatabase();
        db.update(VISITS_TABLE_NAME, cv, KEY_VISIT_ID + " = '" + visitId + "'", null);
        db.close();
    }

    static public void removeVisit(String id) {
        SQLiteDatabase db = new DatabaseService().getWritableDatabase();
        db.delete(VISITS_TABLE_NAME, KEY_VISIT_ID + " = '" + id + "'", null);
        db.close();
    }

    static public List<Visit> getQueringVisits() {
        SQLiteDatabase db = new DatabaseService().getReadableDatabase();
        Cursor cursor = db.query(VISITS_TABLE_NAME, null,
                "(" + KEY_STATUS + "=? OR " + KEY_STATUS + "=?) AND " + KEY_IS_QUERING + "=1",
                new String[]{
                        VisitStatus.SAVED.getName(),
                        VisitStatus.REPORTED.getName()
                },
                null, null, KEY_UPDATED_AT + " ASC");
        ArrayList<Visit> result = new ArrayList<>();
        while (cursor.moveToNext()) {
            Visit visit = getVisitFromCursor(cursor);
            result.add(visit);
        }
        cursor.close();
        db.close();
        return result;
    }

    static public List<Visit> getSavedOrReportedVisits() {
        SQLiteDatabase db = new DatabaseService().getReadableDatabase();
        Cursor cursor = db.query(VISITS_TABLE_NAME, null,
                KEY_STATUS + "=? OR " + KEY_STATUS + "=?",
                new String[]{
                        VisitStatus.SAVED.getName(),
                        VisitStatus.REPORTED.getName()
                },
                null, null, KEY_UPDATED_AT + " ASC");
        ArrayList<Visit> result = new ArrayList<>();
        while (cursor.moveToNext()) {
            Visit visit = getVisitFromCursor(cursor);
            result.add(visit);
        }
        cursor.close();
        db.close();
        return result;
    }

    static public List<Visit> getSavedOrReportedVisits(String date) {
        SQLiteDatabase db = new DatabaseService().getReadableDatabase();
        Cursor cursor = db.query(VISITS_TABLE_NAME, null,
                "(" + KEY_STATUS + "=? OR " + KEY_STATUS + "=?) AND " + KEY_REPORT_DATE + "=?",
                new String[]{
                        VisitStatus.SAVED.getName(),
                        VisitStatus.REPORTED.getName(),
                        date
                },
                null, null, KEY_UPDATED_AT + " ASC");
        ArrayList<Visit> result = new ArrayList<>();
        while (cursor.moveToNext()) {
            Visit visit = getVisitFromCursor(cursor);
            result.add(visit);
        }
        cursor.close();
        db.close();
        return result;
    }

    static public int countSavedOrReportedVisits() {
        return getSavedOrReportedVisits().size();
    }

    static public int countSavedOrReportedVisits(String date) {
        return getSavedOrReportedVisits(date).size();
    }

    static public Visit getQueringVisit() {
        SQLiteDatabase db = new DatabaseService().getReadableDatabase();
        Cursor cursor = db.query(VISITS_TABLE_NAME, null,
                "(" + KEY_STATUS + "=? OR " + KEY_STATUS + "=?) AND " + KEY_IS_QUERING + "=1",
                new String[]{
                        VisitStatus.SAVED.getName(),
                        VisitStatus.REPORTED.getName()
                },
                null, null, KEY_UPDATED_AT + " ASC");
        Visit visit = null;
        if (cursor.moveToFirst()) visit = getVisitFromCursor(cursor);
        cursor.close();
        db.close();
        return visit;
    }

    static public Visit getInProgressVisit(String date) {
        SQLiteDatabase db = new DatabaseService().getReadableDatabase();
        Cursor cursor = db.query(VISITS_TABLE_NAME, null,
                KEY_STATUS + " = ? AND " + KEY_REPORT_DATE + "=?",
                new String[]{
                        VisitStatus.IN_PROGRESS.getName(),
                        date
                },
                null, null, null);
        Visit visit = null;
        if (cursor.moveToFirst()) visit = getVisitFromCursor(cursor);
        cursor.close();
        db.close();
        return visit;
    }

    static public ArrayList<Visit> getVisitsByDate(String date) {
        SQLiteDatabase db = new DatabaseService().getReadableDatabase();
        Cursor cursor = db.query(VISITS_TABLE_NAME, null,
                KEY_REPORT_DATE + " = ?", new String[]{date}, null, null,
                KEY_STATUS_NUMBER + " DESC, " + KEY_UPDATED_AT + " DESC");
        ArrayList<Visit> result = new ArrayList<>();
        while (cursor.moveToNext()) {
            Visit visit = getVisitFromCursor(cursor);
            result.add(visit);
        }
        cursor.close();
        db.close();
        return result;
    }

    static private Visit getVisitFromCursor(Cursor cursor) {
        String id = cursor.getString(cursor.getColumnIndex(KEY_VISIT_ID));
        VisitPlace place = new Gson().fromJson(cursor.getString(cursor.getColumnIndex(KEY_PLACE)),
                VisitPlace.class);
        VisitTemplate template = new Gson().fromJson(
                cursor.getString(cursor.getColumnIndex(KEY_TEMPLATE)), VisitTemplate.class);
        String reportDate = cursor.getString(cursor.getColumnIndex(KEY_REPORT_DATE));
        VisitStatus status = VisitStatus.valueOf(cursor.getString(cursor.getColumnIndex(KEY_STATUS)));
        boolean isAvailableToReport = cursor.getInt(cursor.getColumnIndex(KEY_AVAILABLE_TO_REPORT)) == 1;
        boolean isCheckGeo = cursor.getInt(cursor.getColumnIndex(KEY_IS_CHECK_GEO)) == 1;
        String startTime = cursor.getString(cursor.getColumnIndex(KEY_START_TIME));
        String finishTime = cursor.getString(cursor.getColumnIndex(KEY_FINISH_TIME));
        String startPosition = cursor.getString(cursor.getColumnIndex(KEY_START_POSITION));
        String finishPosition = cursor.getString(cursor.getColumnIndex(KEY_FINISH_POSITION));
        String valuesData = cursor.getString(cursor.getColumnIndex(KEY_VALUES));
        HashMap<String, VisitValue> values = new Gson().fromJson(
                valuesData,
                new TypeToken<HashMap<String, VisitValue>>() {
                }.getType()
        );
        boolean isQuering = cursor.getInt(cursor.getColumnIndex(KEY_IS_QUERING)) == 1;
        String updatedAt = cursor.getString(cursor.getColumnIndex(KEY_UPDATED_AT));

        Visit visit = new Visit(id, place, template, reportDate, status, isAvailableToReport,
                isCheckGeo, startTime, finishTime, startPosition, finishPosition, values, isQuering,
                updatedAt);
        visit.initValues();
        return visit;
    }
}