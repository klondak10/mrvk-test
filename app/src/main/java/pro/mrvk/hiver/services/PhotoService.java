package pro.mrvk.hiver.services;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;

import com.crashlytics.android.Crashlytics;
import com.google.gson.Gson;

import pro.mrvk.hiver.models.UploadVisitsProgress;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.net.UnknownHostException;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import pro.mrvk.hiver.core.API;
import pro.mrvk.hiver.eventbus.RefreshVisitsEvent;
import pro.mrvk.hiver.eventbus.SavedVisitEvent;
import pro.mrvk.hiver.models.Visit;
import pro.mrvk.hiver.models.VisitPhotoValue;
import pro.mrvk.hiver.models.VisitStatus;
import pro.mrvk.hiver.models.VisitUpload;
import pro.mrvk.hiver.network.dto.VisitFinishResDto;
import pro.mrvk.hiver.network.dto.VisitReportResDto;
import pro.mrvk.hiver.network.dto.VisitUploadResDto;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PhotoService extends Service {

    private final IBinder mBinder = new PhotoBinder();

    final int MAX_SEND_TRY_COUNT = 10;

    private UploadVisitsProgress photoCount;
    private boolean isSending = false;
    private int sendTryCount = 0;
    private long startTime;

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        setupEventBus();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        destroyEventBus();
    }

    public class PhotoBinder extends Binder {
        public PhotoService getService() {
            return PhotoService.this;
        }
    }

    private void setupEventBus() {
        EventBus.getDefault().register(this);
    }

    private void destroyEventBus() {
        EventBus.getDefault().unregister(this);
    }

    private void recountUploadProgress() {
        List<Visit> visits = DatabaseService.getSavedOrReportedVisits();
        recountUploadProgress(visits);
    }

    private void recountUploadProgress(List<Visit> visits) {
        photoCount = new UploadVisitsProgress();
        for (Visit visit : visits) {
            String visitId = visit.getId();
            VisitStatus status = visit.getStatus();
            if (status != VisitStatus.SAVED && status != VisitStatus.REPORTED) {
                continue;
            }
            List<VisitUpload> uploads = VisitUpload.of(visit);
            photoCount.setCount(visitId, uploads.size() + 2);
            int sendedCount = visit.getStatus().equals(VisitStatus.REPORTED) ? 1
                    : visit.getStatus().equals(VisitStatus.DONE) ? 2 : 0;
            for (VisitUpload upload : uploads) {
                if (upload.isSended()) sendedCount++;
            }
            photoCount.setSendedCount(visitId, sendedCount);
        }
        EventBus.getDefault().post(photoCount);
    }

    public void sendSavedOrReportedVisits() {
        sendTryCount = 0;
        startTime = new Date().getTime();
//        SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
//        String date = sd.format(new Date());
        List<Visit> visits = DatabaseService.getSavedOrReportedVisits();
        for (Visit visit : visits) {
            if (!visit.isQuering()) {
                visit.setQuering(true);
                DatabaseService.setVisitQuering(visit.getId());
            }
        }
        recountUploadProgress(visits);
        next();
    }

    private void stopSending(String errorMessage) {
        postRefreshVisits();
        if (errorMessage != null) {
//            API.showToast("Отправка отчетов остановлена");
            String error = "Отправка отчетов остановлена. Причина: " + errorMessage;
            Crashlytics.logException(new Exception(error));
            API.showToast(error);
        } else {
            long deltaTime = new Date().getTime() - startTime;
            if (deltaTime > 180_000) {
                API.showToast(String.format(Locale.getDefault(),
                        "Все отчеты отправлены. Отправка заняла %.0f мин.", deltaTime / 180_000f));
            }
        }
    }

    private void next() {
        sendTryCount++;
        Visit visit = DatabaseService.getQueringVisit();
        if (visit == null) {
            stopSending(null);
            return;
        }
        if (visit.getStatus().equals(VisitStatus.SAVED)) {
            reportVisit(visit);
        } else {
            // REPORTED visit
            List<VisitUpload> visitUploads = VisitUpload.of(visit);
            VisitUpload waitUpload = null;
            for (VisitUpload visitUpload : visitUploads) {
                if (!visitUpload.isSended()) {
                    waitUpload = visitUpload;
                    break;
                }
            }
            if (waitUpload != null) {
                uploadPhoto(waitUpload, visit);
            } else {
                finishVisit(visit);
            }
        }
    }

    private void reportVisit(final Visit visit) {
        NetworkService.getInstance().postVisitReport(visit)
                .enqueue(new Callback<VisitReportResDto>() {
                    @Override
                    public void onResponse(Call<VisitReportResDto> call, retrofit2.Response<VisitReportResDto> response) {
                        VisitReportResDto reportResDto = null;
                        if (response.code() == 200) {
                            reportResDto = response.body();
                            if (reportResDto.getError() == null) {
                                sendTryCount = 0;
                                visit.setStatus(VisitStatus.REPORTED);
                                DatabaseService.updateVisitStatus(visit);
                                recountUploadProgress();
                                next();
                                return;
                            }
                        }
                        // error
                        StringBuilder sb = new StringBuilder("Visit = ");
                        Gson gson = new Gson();
                        sb.append(gson.toJson(visit));
                        sb.append("\n\nResponse: ");
                        sb.append(response.code());
                        sb.append(" ");
                        sb.append(response.message());
                        if (reportResDto != null) {
                            sb.append("\n\nError from server: const = ");
                            sb.append(reportResDto.getErrorConst());
                            sb.append(", error = ");
                            sb.append(reportResDto.getError());
                        }
                        if (sendTryCount < MAX_SEND_TRY_COUNT) {
                            next();
                        } else {
                            String errorMessage = "Достигнуто максимальное количество попыток при отправке отчета";
                            stopSending(errorMessage);
                            sb.append("\n\nCrash: ");
                            sb.append(errorMessage);
                        }
                        // send error log to server
                        API.sendLogToServer("REPORT_VISIT FROM MOBILE", new Gson().toJson(visit), "ERROR");
                    }

                    @Override
                    public void onFailure(Call<VisitReportResDto> call, Throwable t) {
                        t.printStackTrace();
                        if (t instanceof UnknownHostException) {
                            // все нормально, нет сети
                            if (sendTryCount < MAX_SEND_TRY_COUNT) {
                                next();
                            } else {
                                stopSending("Нет сети чтобы отправить отчет");
                            }
                            return;
                        }
                        Crashlytics.logException(t);

                        StringBuilder sb = new StringBuilder("Visit = ");
                        Gson gson = new Gson();
                        sb.append(gson.toJson(visit));
                        sb.append("\n\nException = ");
                        sb.append(gson.toJson(t));

                        if (sendTryCount < MAX_SEND_TRY_COUNT) {
                            next();
                        } else {
                            String errorMessage = "CRASH: Достигнуто максимальное количество попыток при отправке отчета";
                            stopSending(errorMessage);
                            sb.append("\n\nCrash: ");
                            sb.append(errorMessage);
                        }
                        // send error log to server
                        API.sendLogToServer("REPORT_VISIT FROM MOBILE", sb.toString(), "EXCEPTION");
                    }
                });
    }


    private void uploadPhoto(final VisitUpload visitUpload, final Visit visit) {
        File photoFile = new File(visitUpload.getPhotoFile());
        RequestBody requestFile = RequestBody.create(MediaType.parse("image/jpeg"), photoFile);
        MultipartBody.Part fileData = MultipartBody.Part.createFormData("photoFile", photoFile.getName(), requestFile);
        NetworkService.getInstance()
                .postVisitUpload(visitUpload.getVisitId(), visitUpload.getBlockId(), visitUpload.getFieldId(), fileData)
                .enqueue(new Callback<VisitUploadResDto>() {
                    @Override
                    public void onResponse(Call<VisitUploadResDto> call, retrofit2.Response<VisitUploadResDto> response) {
                        VisitUploadResDto reportResDto = response.body();
                        if (response.code() == 200 || "ALREADY_UPLOAD_PHOTO".equals(reportResDto.getErrorConst())) {
                            // если сервере сообщает "ALREADY_UPLOAD_PHOTO", значит фото для этого
                            // поля уже было загружено, и повторная отправка не требуется
                            sendTryCount = 0;
                            VisitPhotoValue value = visit.getPhotoValue(visitUpload.getFieldId());
                            if (value == null) {
                                API.showToast("DEBUG: Ошибка хранения данных фотографии", 5);
                                throw new NullPointerException("DEBUG: Ошибка хранения данных фотографии: " + visitUpload.getFieldId() + ", " + visitUpload.getPhotoFile());
                            }
                            value.setSending(true);
                            DatabaseService.updateVisitValues(visit);
                            recountUploadProgress();
                            next();
                        } else {
                            if (sendTryCount < MAX_SEND_TRY_COUNT) {
                                next();
                            } else {
                                stopSending("Достигнуто максимальное количество попыток при загрузке фото");
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<VisitUploadResDto> call, Throwable t) {
                        t.printStackTrace();
                        if (t instanceof UnknownHostException) {
                            // все нормально, нет сети
                            if (sendTryCount < MAX_SEND_TRY_COUNT) {
                                next();
                            } else {
                                stopSending("Нет сети чтобы загрузить фото");
                            }
                            return;
                        }
                        Crashlytics.logException(t);
                        if (sendTryCount < MAX_SEND_TRY_COUNT) {
                            next();
                        } else {
                            stopSending("CRASH: Достигнуто максимальное количество попыток при загрузке фото");
                        }
                    }
                });
    }

    private void finishVisit(final Visit visit) {
        NetworkService.getInstance().postVisitFinish(visit.getId())
                .enqueue(new Callback<VisitFinishResDto>() {
                    @Override
                    public void onResponse(Call<VisitFinishResDto> call, Response<VisitFinishResDto> response) {
                        if (response.code() == 200) {
                            sendTryCount = 0;
                            visit.setStatus(VisitStatus.DONE);
                            visit.setQuering(false);
                            DatabaseService.updateVisitStatus(visit);
                            recountUploadProgress();
                            next();
                        } else {
                            if (sendTryCount < MAX_SEND_TRY_COUNT) {
                                next();
                            } else {
                                stopSending("Достигнуто максимальное количество попыток при закрытии визита");
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<VisitFinishResDto> call, Throwable t) {
                        t.printStackTrace();
                        if (t instanceof UnknownHostException) {
                            // все нормально, нет сети
                            if (sendTryCount < MAX_SEND_TRY_COUNT) {
                                next();
                            } else {
                                stopSending("Нет сети чтобы закрыть визит");
                            }
                            return;
                        }
                        Crashlytics.logException(t);
                        API.showToast("CRASH: Ошибка при закрытии отчета", 5);
                        if (sendTryCount < MAX_SEND_TRY_COUNT) {
                            next();
                        } else {
                            stopSending("CRASH: Достигнуто максимальное количество попыток при закрытии визита");
                        }
                    }
                });
    }

    private void postRefreshVisits() {
        EventBus.getDefault().post(new RefreshVisitsEvent());
    }

    public boolean isSending() {
        return isSending;
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    public void onSavedVisitEvent(SavedVisitEvent event) {
        if (isSending()) {
            Visit visit = event.getVisit();
        }
    }
}
